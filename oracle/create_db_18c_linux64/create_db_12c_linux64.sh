#!/bin/bash
. ~/.bash_profile

export ORACLE_SID=XE
export ORACLE_HOME=/opt/oracle/product/18c/dbhomeXE
export DATAFILES_LOCATION=/opt/oracle/product/18/oradata
export SYS_PASSWORD=c6776c
export SYSTEM_PASSWORD=c6776c
export teco_OWNER=teco
export teco_PASSWORD=c67
export teco_TABLESPACE=TECO_DATA

cp -f init.ora ${ORACLE_HOME}/dbs/init${ORACLE_SID}.ora

rm -f ${ORACLE_HOME}/dbs/orapw${ORACLE_SID}
orapwd file=${ORACLE_HOME}/dbs/orapw${ORACLE_SID} password=${SYS_PASSWORD} entries=3 force=y

#rm -rf ${DATAFILES_LOCATION}/${ORACLE_SID}
mkdir -p ${DATAFILES_LOCATION}/${ORACLE_SID}

#sqlplus "/ as sysdba" @create_db_for_teco.sql ${ORACLE_SID} ${ORACLE_HOME} ${DATAFILES_LOCATION} ${SYS_PASSWORD} ${SYSTEM_PASSWORD}
sqlplus "/ as sysdba" @teco_owner_create.sql ${teco_OWNER} ${teco_PASSWORD} ${teco_TABLESPACE} ${DATAFILES_LOCATION}/${ORACLE_SID}


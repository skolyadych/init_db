spool teco_owner_create

define tecoowner_name=&1
define tecoowner_password=&2
define tecoowner_tablespace=&3
define tecoowner_datafile_path=&4
define tecoowner_datafile=&tecoowner_datafile_path/teco_data01.dbf

set heading  on
set term     on
set feedback on
set echo off
set verify off

---------------------
-- ROLEs
---------------------
alter session set "_ORACLE_SCRIPT"=true;

create role teco_private;
create role teco_user identified by "password";
create role teco_reader identified by "tecoreader";
-- grant connect,resource to teco_private;
grant audit system to teco_private;
grant execute on sys.dbms_session to teco_private;
grant execute on sys.dbms_system to teco_private;
grant teco_private to teco_user;
---------------------
-- Profile
---------------------
create profile tecoprof limit
failed_login_attempts 10;
---------------------
-- Tablespace
---------------------
-- create 
  -- bigfile 
  -- tablespace &&tecoowner_tablespace datafile '&&tecoowner_datafile'
  -- size 100m autoextend on next 100m maxsize unlimited;
---------------------
-- User
---------------------
create user &&tecoowner_name identified by "&&tecoowner_password" --default tablespace &&tecoowner_tablespace; 
---------------------
-- SYS privs
---------------------
@grants_only.sql &&tecoowner_name &&tecoowner_tablespace
---------------------
-- CX_teco context schema
---------------------
@create_context.sql &&tecoowner_name
---------------------

spool off
exit

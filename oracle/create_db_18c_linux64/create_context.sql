define tecoowner_name=&1
define context_password=c67

create user cx_teco identified by &&context_password;
grant connect, resource to cx_teco;
grant create procedure to cx_teco;
alter user cx_teco account lock;

CREATE OR REPLACE PACKAGE cx_teco.pkg_cx_teco
    AUTHID CURRENT_USER
IS
   PROCEDURE setcontext (
      av_context_name   IN   VARCHAR2,
      av_var_name       IN   VARCHAR2,
      av_var_value      IN   VARCHAR2
   );

   PROCEDURE setcx_teco_user_ (groupid IN NUMBER);
END;
/

CREATE OR REPLACE PACKAGE BODY cx_teco.pkg_cx_teco
IS
   PROCEDURE setcontext (
      av_context_name   IN   VARCHAR2,
      av_var_name       IN   VARCHAR2,
      av_var_value      IN   VARCHAR2
   )
   IS
   BEGIN
      DBMS_SESSION.set_context (av_context_name, av_var_name, av_var_value);
   END;

   PROCEDURE setcx_teco_user_groupid (groupid IN NUMBER)
   IS
   BEGIN
      setcontext ('cx_teco_user', 'groupid', TO_CHAR (groupid));
   END;
END;
/

create or replace context cx_teco_user using cx_teco.pkg_cx_teco;
create synonym &&tecoowner_name..pkg_cx_teco for cx_teco.pkg_cx_teco;

grant execute on cx_teco.pkg_cx_teco to &&tecoowner_name;
grant execute on cx_teco.pkg_cx_teco to teco_private,tecoreader;

create role tecoapp      identified using cx_teco.pki_settecorole;
create role tecoapp_ldap identified using cx_teco.pki_SettecoRole_ldap;

grant teco_private to tecoapp,tecoapp_ldap;

create or replace context factoring_ctx using creator.setcontext;
create or replace context factoringctx  using creator.setcontext;

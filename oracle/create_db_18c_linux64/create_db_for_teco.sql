spool create_&1
define SID=&1
define OH=&2
define FD=&3
define sysPassword=&4
define systemPassword=&5

set echo on

shutdown abort

create spfile from pfile;

startup nomount

-- Regular files
create database
  USER SYS IDENTIFIED BY &&sysPassword
  USER SYSTEM IDENTIFIED BY &&systemPassword
maxlogmembers 5
maxdatafiles 200
maxinstances 2
character set CL8MSWIN1251
noarchivelog
set default smallfile tablespace
datafile 
  '&&FD/&&SID/system01.dbf' size 400M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
logfile 
  group 1 '&&FD/&&SID/redo01.log' size 500M, 
  group 2 '&&FD/&&SID/redo02.log' size 500M, 
  group 3 '&&FD/&&SID/redo03.log' size 500M,
  group 4 '&&FD/&&SID/redo04.log' size 500M, 
  group 5 '&&FD/&&SID/redo05.log' size 500M
sysaux datafile 
  '&&FD/&&SID/sysaux01.dbf' 
  size 196M AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED
default temporary tablespace TEMP tempfile 
  '&&FD/&&SID/temp01.dbf' 
  size 196M AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED
undo tablespace UNDOTBS1 datafile 
  '&&FD/&&SID/undo01.dbf' 
  size 196M AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED
default tablespace USERS datafile 
  '&&FD/&&SID/users01.dbf' 
  size 196M AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED;
spool off

spool create_add_&&SID

alter session set "_ORACLE_SCRIPT" = true;

-- CATALOG CATPROC XDB
@&&OH/rdbms/admin/catalog.sql
@&&OH/rdbms/admin/catproc.sql

-- Help system
connect SYSTEM/&&systemPassword
@&&OH/sqlplus/admin/pupbld.sql
@&&OH/sqlplus/admin/help/hlpbld.sql helpus.sql

-- JVM
connect SYS/&&sysPassword as sysdba
@&&OH/javavm/install/initjvm.sql
@&&OH/xdk/admin/initxml.sql
@&&OH/rdbms/admin/catjava.sql
@&&OH/rdbms/admin/catxdbj.sql

-- compile
@&&OH/rdbms/admin/utlrp.sql

alter session set "_ORACLE_SCRIPT" = false;

-- apply PSU
shutdown immediate
startup upgrade
host cd &&OH/OPatch
host sh datapatch -verbose
shutdown immediate
startup

-- compile
@&&OH/rdbms/admin/utlrp.sql

spool off
exit

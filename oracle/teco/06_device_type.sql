/* Formatted on 23-���-2019 22:27:12 (QP5 v5.336) */
/*
device_type
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.device_type', 'table');
    silent_drop ('seq_device_type_id', 'sequence');

    COMMIT;
END;

CREATE SEQUENCE seq_device_type_id START WITH 1;



CREATE TABLE device_type
(
    id                        NUMBER (10, 0) NOT NULL CONSTRAINT pk_device_type_pk PRIMARY KEY,
    code                      VARCHAR2 (32) NOT NULL CONSTRAINT uq_device_type_code UNIQUE,
    model                     VARCHAR2 (512) NOT NULL,
    description               VARCHAR2 (512) NULL,
    vendor                    VARCHAR2 (128) NULL,
    device_class_code         VARCHAR2 (32) NOT NULL,
    utility_path              VARCHAR2 (1024) NULL,
    interface_type_code       VARCHAR2 (32) NOT NULL,
    port_speed                NUMBER (15, 0) NULL,
    data_bits                 NUMBER (10, 0) NULL,
    is_parity                 NUMBER (1, 0) DEFAULT 0,
    is_flow_control           NUMBER (1, 0) DEFAULT 0,
    uart_driver_type          VARCHAR2 (32) NULL,
    is_hw_virtual_port_use    NUMBER (1, 0) NOT NULL,
    is_termo_connected        NUMBER (1, 0) DEFAULT 0,
    configuration_code        VARCHAR2 (32) NOT NULL,
    created                   DATE DEFAULT SYSDATE,
    creator_code              VARCHAR2 (32) NOT NULL,
    changed                    DATE DEFAULT SYSDATE,
    changer_code              VARCHAR2 (32) NOT NULL,
    is_deleted                NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT fk_dt_device_class_code FOREIGN KEY (device_class_code)
        REFERENCES teco.device_class (code),
    CONSTRAINT fk_dt_interface_type_code FOREIGN KEY (interface_type_code)
        REFERENCES teco.interface_type (code),
    CONSTRAINT fk_dt_device_type_conf_code FOREIGN KEY (configuration_code)
        REFERENCES device_config (code),
    CONSTRAINT fk_device_type_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_device_type_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_device_type_id
    BEFORE INSERT
    ON device_type
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_device_type_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE device_type IS
    '���� ��������� (�������)';

COMMENT ON COLUMN device_type.id IS 'Primary key';

COMMENT ON COLUMN device_type.model IS '������ ����������';

COMMENT ON COLUMN device_type.description IS '�������� ������';

COMMENT ON COLUMN device_type.vendor IS '�������������';

COMMENT ON COLUMN device_type.device_class_code IS
    'id ������ ���������';

COMMENT ON COLUMN device_type.utility_path IS 'vendor utility path';

COMMENT ON COLUMN device_type.interface_type_code IS
    '�������� ����� ����������';

COMMENT ON COLUMN device_type.port_speed IS
    '�������� ����� ���������� ���\���';

COMMENT ON COLUMN device_type.data_bits IS '�������� ���\���';

COMMENT ON COLUMN device_type.is_parity IS '��������';

COMMENT ON COLUMN device_type.is_flow_control IS
    '���������� ������� (��\���)';

COMMENT ON COLUMN device_type.uart_driver_type IS
    '��� �������� UART';

COMMENT ON COLUMN device_type.is_hw_virtual_port_use IS
    '������������ ����������� HW ����';

COMMENT ON COLUMN device_type.is_termo_connected IS
    '����������� �����������';

COMMENT ON COLUMN device_type.configuration_code IS
    'id ������������ ��� ���� ������������';



CREATE INDEX device_type_model_uindex
    ON device_type (model);



GRANT ALL ON teco.users TO teco_user;
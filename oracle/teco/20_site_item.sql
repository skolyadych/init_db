/* Formatted on 24-���-2019 10:09:39 (QP5 v5.336) */
/*
site_item
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.site_item', 'table');
    silent_drop ('seq_site_item_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_site_item_id START WITH 1;

CREATE TABLE site_item
(
    id                 NUMBER (10, 0) NOT NULL CONSTRAINT pk_site_item_id PRIMARY KEY,
    code               VARCHAR2 (32) NOT NULL CONSTRAINT uq_site_item_code UNIQUE,
    site_code          VARCHAR2 (32) NOT NULL,
    port_no            NUMBER (3, 0) NOT NULL,
    device_code        VARCHAR2 (32) NOT NULL,
    interface_code     VARCHAR2 (32) NOT NULL,
    status_code        VARCHAR2 (32) NOT NULL,
    last_alarm_code    NUMBER (3, 0) NOT NULL,
    ip_address         VARCHAR2 (128) NOT NULL,
    ip_pool_code       VARCHAR2 (32) NOT NULL,
    created            DATE DEFAULT SYSDATE,
    creator_code       VARCHAR2 (32) NOT NULL,
    changed            DATE DEFAULT SYSDATE,
    changer_code       VARCHAR2 (32) NOT NULL,
    is_deleted         NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fk_si_site_code FOREIGN KEY (site_code)
        REFERENCES teco.site (code),
    CONSTRAINT fk_si_device_code FOREIGN KEY (device_code)
        REFERENCES teco.device (code),
    CONSTRAINT fk_si_interface_code FOREIGN KEY (interface_code)
        REFERENCES teco.interface_type (code),
    CONSTRAINT fk_si_status_code FOREIGN KEY (status_code)
        REFERENCES teco.device_status (code),
    CONSTRAINT fk_si_ip_pool_code FOREIGN KEY (ip_pool_code)
        REFERENCES teco.dhcp_pool_link (code),
    CONSTRAINT fk_site_item_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_site_item_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_site_item_id
    BEFORE INSERT
    ON site_item
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_site_item_id.NEXTVAL INTO :new.id FROM DUAL;
END;


COMMENT ON TABLE site_item IS '���������� � �����';

COMMENT ON COLUMN site_item.id IS 'Primary Key';

COMMENT ON COLUMN site_item.site_code IS 'Reference to site';

COMMENT ON COLUMN site_item.port_no IS 'Number of port on device';

COMMENT ON COLUMN site_item.device_code IS
    '���������� � �����';

COMMENT ON COLUMN site_item.status_code IS
    '�� ������� ����������';

COMMENT ON COLUMN site_item.last_alarm_code IS
    '��������� ��� �������';



CREATE UNIQUE INDEX site_item_siteid_portno
    ON site_item (status_code, port_no);

GRANT ALL ON teco.site_item TO teco_user;
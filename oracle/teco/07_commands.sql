/* Formatted on 23-���-2019 22:45:54 (QP5 v5.336) */
/*
commands
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.commands', 'table');
    silent_drop ('seq_commands_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_commands_id START WITH 1;

CREATE TABLE commands
(
    id                    NUMBER (10, 0) NOT NULL CONSTRAINT pk_commands_pk PRIMARY KEY,
    code                  VARCHAR2 (32) NOT NULL CONSTRAINT uq_commands_code UNIQUE,
    configuration_code    VARCHAR2 (32) NOT NULL,
    direction             NUMBER (1, 0) NOT NULL,
    order_no              NUMBER (5, 0) NOT NULL,
    sname                 VARCHAR2 (128) NOT NULL,
    description           VARCHAR2 (512) NULL,
    created               DATE DEFAULT SYSDATE,
    creator_code          VARCHAR2 (32) NOT NULL,
    changed               DATE DEFAULT SYSDATE,
    changer_code          VARCHAR2 (32) NOT NULL,
    is_deleted            NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT uq_cc_config_order UNIQUE (configuration_code, order_no),
    CONSTRAINT fk_cc_configuration_code FOREIGN KEY (configuration_code)
        REFERENCES device_config (code),
    CONSTRAINT fk_commands_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_commands_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_commands_id
    BEFORE INSERT
    ON commands
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_commands_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE commands IS
    '�������� ��� ���������� ������������';

COMMENT ON COLUMN commands.id IS 'Primary Key';

COMMENT ON COLUMN commands.id IS 'Primary Key';

COMMENT ON COLUMN commands.direction IS
    '����������� ��������
0-��������
1-���������';

COMMENT ON COLUMN commands.order_no IS '������� ����������';
COMMENT ON COLUMN device_config.sname IS 'ShortName';

COMMENT ON COLUMN device_config.description IS
    '�������� ������������';

GRANT ALL ON teco.commands TO teco_user;
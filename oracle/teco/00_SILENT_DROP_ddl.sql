
CREATE OR REPLACE 
PROCEDURE silent_drop (obj_name IN VARCHAR2, obj_type VARCHAR2)
AS
BEGIN
    IF LOWER (obj_type) = 'table'
    THEN
        BEGIN
            FOR rec IN (SELECT *
                        FROM user_constraints c
                        WHERE c.table_name = obj_name)
            LOOP
                DBMS_OUTPUT.put_line (
                       'ALTER TABLE  '
                    || obj_name
                    || 'DROP CONSTRAINT '
                    || rec.constraint_name);

                EXECUTE IMMEDIATE   'ALTER TABLE  '
                                 || obj_name
                                 || 'DROP CONSTRAINT '
                                 || rec.constraint_name;

                DBMS_OUTPUT.put_line (rec.constraint_name);

                COMMIT;
            END LOOP;

            EXECUTE IMMEDIATE   'DROP '
                             || obj_type
                             || ' '
                             || obj_name
                             || ' CASCADE CONSTRAINTS';
        END;
    ELSE
        BEGIN
            EXECUTE IMMEDIATE 'DROP ' || obj_type || ' ' || obj_name;
        END;
    END IF;
EXCEPTION
    WHEN OTHERS
    THEN
        IF SQLCODE NOT IN (-942,                                      -- table
                                 -2289)                            -- sequence
        THEN
            DBMS_OUTPUT.put_line (obj_name);
            RAISE;
        END IF;
END;
/





/* Formatted on 23-���-2019 23:26:39 (QP5 v5.336) */
/*
scenario_step_params
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.scenario_step_params', 'table');
    silent_drop ('seq_scenario_step_params_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_scenario_step_params_id START WITH 1;


CREATE TABLE scenario_step_params
(
    id                NUMBER (10, 0)
                         NOT NULL
                         CONSTRAINT pk_scenario_step_params_id PRIMARY KEY,
    code              VARCHAR2 (32)
                         NOT NULL
                         CONSTRAINT uq_scenario_step_params_code UNIQUE,
    sname             VARCHAR2 (128) NOT NULL,
    description       VARCHAR2 (512) NULL,
    scenario_code     VARCHAR2 (32) NOT NULL,
    param_types       VARCHAR2 (32) NOT NULL,
    is_mandatory      NUMBER (1, 1) DEFAULT 0 NOT NULL,
    param_order_no    NUMBER (3, 0) NOT NULL,
    created           DATE DEFAULT SYSDATE,
    creator_code      VARCHAR2 (32) NOT NULL,
    chaged            DATE DEFAULT SYSDATE,
    changer_code      VARCHAR2 (32) NOT NULL,
    is_deleted        NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT uq_param_order_scenario UNIQUE (scenario_code, param_order_no),
    CONSTRAINT fk_sp_scenario_code FOREIGN KEY (scenario_code)
        REFERENCES teco.scenarios (code),
    CONSTRAINT fk_sp_param_types_code FOREIGN KEY (param_types)
        REFERENCES teco.param_types (code),
    CONSTRAINT fk_scenario_step_params_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_scenario_step_params_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_scenario_step_params_id
    BEFORE INSERT
    ON scenario_step_params
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_scenario_step_params_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE scenario_step_params IS
    '��������� ����� �������';

COMMENT ON COLUMN scenario_step_params.id IS 'PrimaryKey';

COMMENT ON COLUMN scenario_step_params.sname IS
    '�������� ���������';

COMMENT ON COLUMN scenario_step_params.description IS
    '�������� ���������';

COMMENT ON COLUMN scenario_step_params.scenario_code IS
    '�������� � ��������';

COMMENT ON COLUMN scenario_step_params.param_types IS
    '��� ���������';

COMMENT ON COLUMN scenario_step_params.is_mandatory IS
    '�������������� ���������';

CREATE INDEX scenario_step_params_sname_uindex
    ON scenario_step_params (sname);

GRANT ALL ON scenario_step_params TO teco_user;
/* Formatted on 23-���-2019 23:15:02 (QP5 v5.336) */
/*
param_types
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.param_types', 'table');
    silent_drop ('seq_param_types_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_param_types_id START WITH 1;

CREATE TABLE param_types
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_param_types_id PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_param_types_code UNIQUE,
    sname           VARCHAR (128) NOT NULL,
    description     VARCHAR (512) NULL,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR (32) NOT NULL,
    changed         DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMERIC (1, 0) DEFAULT 0 NOT NULL,
    CONSTRAINT fk_param_types_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_param_types_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_param_types_id
    BEFORE INSERT
    ON param_types
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_param_types_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE param_types IS '���� ����������';

COMMENT ON COLUMN param_types.id IS 'PrimaryKey';

COMMENT ON COLUMN param_types.sname IS '�������� ���������';

COMMENT ON COLUMN param_types.description IS
    '�������� ���������';


GRANT ALL ON param_types TO teco_user;
/* Formatted on 24-���-2019 08:50:18 (QP5 v5.336) */
/*
dhcp_pool_link
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.dhcp_pool_link', 'table');
    silent_drop ('seq_dhcp_pool_link_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_dhcp_pool_link_id START WITH 1;

CREATE TABLE teco.dhcp_pool_link
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT dhcp_pool_link PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_dhcp_pool_link_code UNIQUE,
    netmask         VARCHAR2 (128) NOT NULL,
    bits            VARCHAR2 (32) NOT NULL,
    gateway         VARCHAR2 (32) NOT NULL,
    lease_time      NUMBER (10, 0) NOT NULL,
    region_code     VARCHAR2 (32) NOT NULL,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT fk_dpl_region_code FOREIGN KEY (region_code)
        REFERENCES regions (code),
    CONSTRAINT fk_dhcp_pool_link_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_dhcp_pool_link_id
    BEFORE INSERT
    ON dhcp_pool_link
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_dhcp_pool_link_id.NEXTVAL INTO :new.id FROM DUAL;
END;


COMMENT ON TABLE dhcp_pool_link IS 'L3 ����  ��������';

COMMENT ON COLUMN dhcp_pool_link.id IS 'PrimaryKey';

COMMENT ON COLUMN dhcp_pool_link.netmask IS '����� ����';

COMMENT ON COLUMN dhcp_pool_link.bits IS
    '���������� ������� ';
COMMENT ON COLUMN dhcp_pool_link.gateway IS '����';
COMMENT ON COLUMN dhcp_pool_link.lease_time IS '����� ������';

CREATE INDEX dhcp_pool_link_netmask_uindex
    ON dhcp_pool_link (netmask);


GRANT ALL ON dhcp_pool_link TO teco_user;
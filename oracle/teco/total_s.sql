/* Formatted on 24-���-2019 10:10:58 (QP5 v5.336) */
/*
Ueser Roles
v 1.0
*/

CREATE OR REPLACE 
PROCEDURE silent_drop (obj_name IN VARCHAR2, obj_type VARCHAR2)
AS
BEGIN
    IF LOWER (obj_type) = 'table'
    THEN
        BEGIN
            FOR rec IN (SELECT *
                        FROM user_constraints c
                        WHERE c.table_name = obj_name)
            LOOP
                DBMS_OUTPUT.put_line (
                       'ALTER TABLE  '
                    || obj_name
                    || 'DROP CONSTRAINT '
                    || rec.constraint_name);

                EXECUTE IMMEDIATE   'ALTER TABLE  '
                                 || obj_name
                                 || 'DROP CONSTRAINT '
                                 || rec.constraint_name;

                DBMS_OUTPUT.put_line (rec.constraint_name);

                COMMIT;
            END LOOP;

            EXECUTE IMMEDIATE   'DROP '
                             || obj_type
                             || ' '
                             || obj_name
                             || ' CASCADE CONSTRAINTS';
        END;
    ELSE
        BEGIN
            EXECUTE IMMEDIATE 'DROP ' || obj_type || ' ' || obj_name;
        END;
    END IF;
EXCEPTION
    WHEN OTHERS
    THEN
        IF SQLCODE NOT IN (-942,                                      -- table
                                 -2289)                            -- sequence
        THEN
            DBMS_OUTPUT.put_line (obj_name);
            RAISE;
        END IF;
END;


BEGIN
    silent_drop ('teco.user_roles', 'table');
    silent_drop ('seq_user_roles_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_user_roles_id START WITH 1;

CREATE TABLE user_roles
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT user_roles_pk PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL,
    role_name       VARCHAR2 (128) NOT NULL,
    domain_group    VARCHAR2 (512) NULL,
    description     VARCHAR2 (512) NULL,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    changed         DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT uq_user_roles_code UNIQUE (code)
);

CREATE OR REPLACE TRIGGER tr_bi_user_roles_id
    BEFORE INSERT
    ON user_roles
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_user_roles_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE user_roles IS '������ ������������';

COMMENT ON COLUMN user_roles.id IS 'Primary key';

COMMENT ON COLUMN user_roles.role_name IS 'Role name';

COMMENT ON COLUMN user_roles.domain_group IS 'ActiveDirectory group';

COMMENT ON COLUMN user_roles.description IS 'RoleDescription';


GRANT ALL ON teco.user_roles TO teco_user;


INSERT INTO teco.user_roles (code,
                             role_name,
                             domain_group,
                             description,
                             creator_code,
                             changer_code)
VALUES ('owner',
        'teco_owner',
        'KS_Teco_owners',
        'Teco Developpers',
        'owner',
        'owner');

COMMIT;

/*
Uesers
v 1.0
*/


BEGIN
    silent_drop ('teco.users', 'table');
    silent_drop ('seq_users_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_users_id START WITH 1;

CREATE TABLE teco.users
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT users_pk PRIMARY KEY, --primary key
    login           VARCHAR2 (128) NOT NULL,
    first_name      VARCHAR2 (128) NOT NULL,
    last_name       VARCHAR2 (128) NOT NULL,
    middle_name     VARCHAR2 (128) NULL,
    group_code      VARCHAR2 (32) NOT NULL,
    position        VARCHAR2 (512) NULL,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NULL,
    changed         DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NULL,
    is_deleted      NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT uq_users_name UNIQUE (login),
    CONSTRAINT fk_user_roles_code FOREIGN KEY (group_code)
        REFERENCES user_roles (code) ON DELETE CASCADE
);


CREATE OR REPLACE TRIGGER tr_bi_users_id
    BEFORE INSERT
    ON users
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_users_id.NEXTVAL INTO :new.id FROM DUAL;
END;



GRANT ALL ON teco.users TO teco_user;

INSERT INTO users (login,
                   first_name,
                   last_name,
                   middle_name,
                   position,
                   group_code)
VALUES ('admin',
        'Sergey',
        'Kolyadich',
        'Arkadievich',
        'Developper',
        'owner');

INSERT INTO users (login,
                   first_name,
                   last_name,
                   middle_name,
                   position,
                   group_code)
VALUES ('teco',
        'creator',
        'teco',
        'teco',
        'dbowner',
        'owner');


COMMIT;

/*
device_class
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.device_class', 'table');
    silent_drop ('seq_device_class_id', 'sequence');
    silent_drop ('seq_device_class_order_no', 'sequence');
    COMMIT;
END;

CREATE SEQUENCE seq_device_class_id START WITH 1;

CREATE SEQUENCE seq_device_class_order_no START WITH 1;

CREATE TABLE teco.device_class
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_device_class PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_device_class_code UNIQUE,
    sname           VARCHAR2 (128) NOT NULL,
    description     VARCHAR2 (512) NULL,
    order_no        NUMBER (10, 0)
                       NOT NULL
                       CONSTRAINT uq_device_class_order UNIQUE,
    request_rate    NUMBER (15, 0) DEFAULT 0,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    chaged          DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT fk_device_class_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_device_class_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);


CREATE OR REPLACE TRIGGER tr_bi_device_class_id
    BEFORE INSERT
    ON device_class
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_device_class_id.NEXTVAL INTO :new.id FROM DUAL;
END;

CREATE OR REPLACE TRIGGER tr_bi_device_class_order_no
    BEFORE INSERT
    ON device_class
    FOR EACH ROW
    WHEN (new.order_no IS NULL)
BEGIN
    SELECT seq_device_class_order_no.NEXTVAL INTO :new.order_no FROM DUAL;
END;



COMMENT ON TABLE device_class IS '������ ���������';

COMMENT ON COLUMN device_class.id IS 'Primary key';

COMMENT ON COLUMN device_class.sname IS
    '�������� ��� ��� ������ ����';

COMMENT ON COLUMN device_class.description IS
    '�������� ������ ��������� (��� ����������� ���������';

COMMENT ON COLUMN device_class.order_no IS
    '���������� ����� (��� ����������)';

COMMENT ON COLUMN device_class.request_rate IS
    '������� ������ �� ��������� � �������� (0-�� ����������)';


GRANT ALL ON teco.users TO teco_user;


/*
interface_type
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.interface_type', 'table');
    silent_drop ('seq_interface_type_id', 'sequence');

    COMMIT;
END;

CREATE SEQUENCE seq_interface_type_id START WITH 1;


CREATE TABLE interface_type
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_interface_type_pk PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_interface_type_code UNIQUE,
    sname           VARCHAR2 (128) NOT NULL,
    description     VARCHAR2 (512) NULL,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT fk_interface_type_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE
);


CREATE OR REPLACE TRIGGER tr_bi_interface_type_id
    BEFORE INSERT
    ON interface_type
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_interface_type_id.NEXTVAL INTO :new.id FROM DUAL;
END;


COMMENT ON TABLE interface_type IS
    '���� ����������� ���������';

COMMENT ON COLUMN interface_type.id IS 'PrimaryKey';

COMMENT ON COLUMN interface_type.sname IS
    '�������� �����������';

COMMENT ON COLUMN interface_type.description IS
    '�������� ���������';


GRANT ALL ON interface_type TO teco_user;


/*
device_configuration
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.device_config', 'table');
    silent_drop ('seq_device_config_id', 'sequence');

    COMMIT;
END;

CREATE SEQUENCE seq_device_config_id START WITH 1;



CREATE TABLE device_config
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_device_config_pk PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_device_config_code UNIQUE,
    sname           VARCHAR2 (128) NOT NULL,
    description     VARCHAR2 (512),
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    changed         DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fk_device_config_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_device_config_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);


CREATE OR REPLACE TRIGGER tr_bi_device_config_id
    BEFORE INSERT
    ON device_config
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_device_config_id.NEXTVAL INTO :new.id FROM DUAL;
END;


COMMENT ON TABLE device_config IS
    '������������ ����� ���������';

COMMENT ON COLUMN device_config.id IS 'Primary Key';

COMMENT ON COLUMN device_config.sname IS 'ShortName';

COMMENT ON COLUMN device_config.description IS
    '�������� ������������';


GRANT ALL ON teco.device_config TO teco_user;

/*
device_type
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.device_type', 'table');
    silent_drop ('seq_device_type_id', 'sequence');

    COMMIT;
END;

CREATE SEQUENCE seq_device_type_id START WITH 1;



CREATE TABLE device_type
(
    id                        NUMBER (10, 0) NOT NULL CONSTRAINT pk_device_type_pk PRIMARY KEY,
    code                      VARCHAR2 (32) NOT NULL CONSTRAINT uq_device_type_code UNIQUE,
    model                     VARCHAR2 (512) NOT NULL,
    description               VARCHAR2 (512) NULL,
    vendor                    VARCHAR2 (128) NULL,
    device_class_code         VARCHAR2 (32) NOT NULL,
    utility_path              VARCHAR2 (1024) NULL,
    interface_type_code       VARCHAR2 (32) NOT NULL,
    port_speed                NUMBER (15, 0) NULL,
    data_bits                 NUMBER (10, 0) NULL,
    is_parity                 NUMBER (1, 0) DEFAULT 0,
    is_flow_control           NUMBER (1, 0) DEFAULT 0,
    uart_driver_type          VARCHAR2 (32) NULL,
    is_hw_virtual_port_use    NUMBER (1, 0) NOT NULL,
    is_termo_connected        NUMBER (1, 0) DEFAULT 0,
    configuration_code        VARCHAR2 (32) NOT NULL,
    created                   DATE DEFAULT SYSDATE,
    creator_code              VARCHAR2 (32) NOT NULL,
    changed                   DATE DEFAULT SYSDATE,
    changer_code              VARCHAR2 (32) NOT NULL,
    is_deleted                NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT fk_dt_device_class_code FOREIGN KEY (device_class_code)
        REFERENCES teco.device_class (code),
    CONSTRAINT fk_dt_interface_type_code FOREIGN KEY (interface_type_code)
        REFERENCES teco.interface_type (code),
    CONSTRAINT fk_dt_device_type_conf_code FOREIGN KEY (configuration_code)
        REFERENCES device_config (code),
    CONSTRAINT fk_device_type_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_device_type_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_device_type_id
    BEFORE INSERT
    ON device_type
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_device_type_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE device_type IS
    '���� ��������� (�������)';

COMMENT ON COLUMN device_type.id IS 'Primary key';

COMMENT ON COLUMN device_type.model IS '������ ����������';

COMMENT ON COLUMN device_type.description IS '�������� ������';

COMMENT ON COLUMN device_type.vendor IS '�������������';

COMMENT ON COLUMN device_type.device_class_code IS
    'id ������ ���������';

COMMENT ON COLUMN device_type.utility_path IS 'vendor utility path';

COMMENT ON COLUMN device_type.interface_type_code IS
    '�������� ����� ����������';

COMMENT ON COLUMN device_type.port_speed IS
    '�������� ����� ���������� ���\���';

COMMENT ON COLUMN device_type.data_bits IS '�������� ���\���';

COMMENT ON COLUMN device_type.is_parity IS '��������';

COMMENT ON COLUMN device_type.is_flow_control IS
    '���������� ������� (��\���)';

COMMENT ON COLUMN device_type.uart_driver_type IS
    '��� �������� UART';

COMMENT ON COLUMN device_type.is_hw_virtual_port_use IS
    '������������ ����������� HW ����';

COMMENT ON COLUMN device_type.is_termo_connected IS
    '����������� �����������';

COMMENT ON COLUMN device_type.configuration_code IS
    'id ������������ ��� ���� ������������';



CREATE INDEX device_type_model_uindex
    ON device_type (model);



GRANT ALL ON teco.users TO teco_user;

/*
commands
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.commands', 'table');
    silent_drop ('seq_commands_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_commands_id START WITH 1;

CREATE TABLE commands
(
    id                    NUMBER (10, 0) NOT NULL CONSTRAINT pk_commands_pk PRIMARY KEY,
    code                  VARCHAR2 (32) NOT NULL CONSTRAINT uq_commands_code UNIQUE,
    configuration_code    VARCHAR2 (32) NOT NULL,
    direction             NUMBER (1, 0) NOT NULL,
    order_no              NUMBER (5, 0) NOT NULL,
    sname                 VARCHAR2 (128) NOT NULL,
    description           VARCHAR2 (512) NULL,
    created               DATE DEFAULT SYSDATE,
    creator_code          VARCHAR2 (32) NOT NULL,
    changed               DATE DEFAULT SYSDATE,
    changer_code          VARCHAR2 (32) NOT NULL,
    is_deleted            NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT uq_cc_config_order UNIQUE (configuration_code, order_no),
    CONSTRAINT fk_cc_configuration_code FOREIGN KEY (configuration_code)
        REFERENCES device_config (code),
    CONSTRAINT fk_commands_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_commands_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_commands_id
    BEFORE INSERT
    ON commands
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_commands_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE commands IS
    '�������� ��� ���������� ������������';

COMMENT ON COLUMN commands.id IS 'Primary Key';

COMMENT ON COLUMN commands.id IS 'Primary Key';

COMMENT ON COLUMN commands.direction IS
    '����������� ��������
0-��������
1-���������';

COMMENT ON COLUMN commands.order_no IS '������� ����������';
COMMENT ON COLUMN device_config.sname IS 'ShortName';

COMMENT ON COLUMN device_config.description IS
    '�������� ������������';

GRANT ALL ON teco.commands TO teco_user;

/*
commands
v 1.0
S.K.
*/


BEGIN
    silent_drop ('teco.scenarios', 'table');
    silent_drop ('seq_scenarios_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_scenarios_id START WITH 1;

CREATE TABLE scenarios
(
    id                  NUMBER (10, 0) NOT NULL CONSTRAINT pk_scenarios_id PRIMARY KEY,
    code                VARCHAR2 (32) NOT NULL CONSTRAINT uq_scenarios_code UNIQUE,
    command_code        VARCHAR2 (32) NOT NULL,
    sname               VARCHAR2 (128) NOT NULL,
    step                NUMBER (3, 0) NOT NULL,
    request_pattern     VARCHAR2 (2048) NULL,
    responce_pattern    VARCHAR2 (2048) NULL,
    description         VARCHAR2 (512) NULL,
    created             DATE DEFAULT SYSDATE,
    creator_code        VARCHAR2 (32) NOT NULL,
    changed             DATE DEFAULT SYSDATE,
    changer_code        VARCHAR2 (32) NULL,
    is_deleted          NUMBER (1, 0) DEFAULT 0 NOT NULL,
    CONSTRAINT uq_scenarios_order_com UNIQUE (command_code, step),
    CONSTRAINT fk_sc_command_code FOREIGN KEY (command_code)
        REFERENCES teco.commands (code),
    CONSTRAINT fk_scenarios_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_scenarios_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);


CREATE OR REPLACE TRIGGER tr_bi_scenarios_id
    BEFORE INSERT
    ON scenarios
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_scenarios_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE scenarios IS '�������� �������';

COMMENT ON COLUMN scenarios.id IS 'Primary key';

COMMENT ON COLUMN scenarios.command_code IS 'Reference to commands';

COMMENT ON COLUMN scenarios.sname IS
    '�������� ���� ��������';

COMMENT ON COLUMN scenarios.step IS '��� ��������';

COMMENT ON COLUMN scenarios.request_pattern IS 'Request pattern';

COMMENT ON COLUMN scenarios.responce_pattern IS 'Responce pattern';

COMMENT ON COLUMN scenarios.description IS '�������� ����';


GRANT ALL ON teco.scenarios TO teco_user;


/*
param_types
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.param_types', 'table');
    silent_drop ('seq_param_types_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_param_types_id START WITH 1;

CREATE TABLE param_types
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_param_types_id PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_param_types_code UNIQUE,
    sname           VARCHAR (128) NOT NULL,
    description     VARCHAR (512) NULL,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR (32) NOT NULL,
    changed         DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMERIC (1, 0) DEFAULT 0 NOT NULL,
    CONSTRAINT fk_param_types_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_param_types_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_param_types_id
    BEFORE INSERT
    ON param_types
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_param_types_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE param_types IS '���� ����������';

COMMENT ON COLUMN param_types.id IS 'PrimaryKey';

COMMENT ON COLUMN param_types.sname IS '�������� ���������';

COMMENT ON COLUMN param_types.description IS
    '�������� ���������';


GRANT ALL ON param_types TO teco_user;

/*
scenario_step_params
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.scenario_step_params', 'table');
    silent_drop ('seq_scenario_step_params_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_scenario_step_params_id START WITH 1;


CREATE TABLE scenario_step_params
(
    id                NUMBER (10, 0)
                         NOT NULL
                         CONSTRAINT pk_scenario_step_params_id PRIMARY KEY,
    code              VARCHAR2 (32)
                         NOT NULL
                         CONSTRAINT uq_scenario_step_params_code UNIQUE,
    sname             VARCHAR2 (128) NOT NULL,
    description       VARCHAR2 (512) NULL,
    scenario_code     VARCHAR2 (32) NOT NULL,
    param_types       VARCHAR2 (32) NOT NULL,
    is_mandatory      NUMBER (1, 1) DEFAULT 0 NOT NULL,
    param_order_no    NUMBER (3, 0) NOT NULL,
    created           DATE DEFAULT SYSDATE,
    creator_code      VARCHAR2 (32) NOT NULL,
    chaged            DATE DEFAULT SYSDATE,
    changer_code      VARCHAR2 (32) NOT NULL,
    is_deleted        NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT uq_param_order_scenario UNIQUE (scenario_code, param_order_no),
    CONSTRAINT fk_sp_scenario_code FOREIGN KEY (scenario_code)
        REFERENCES teco.scenarios (code),
    CONSTRAINT fk_sp_param_types_code FOREIGN KEY (param_types)
        REFERENCES teco.param_types (code),
    CONSTRAINT fk_scenario_step_params_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_scenario_step_params_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_scenario_step_params_id
    BEFORE INSERT
    ON scenario_step_params
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_scenario_step_params_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE scenario_step_params IS
    '��������� ����� �������';

COMMENT ON COLUMN scenario_step_params.id IS 'PrimaryKey';

COMMENT ON COLUMN scenario_step_params.sname IS
    '�������� ���������';

COMMENT ON COLUMN scenario_step_params.description IS
    '�������� ���������';

COMMENT ON COLUMN scenario_step_params.scenario_code IS
    '�������� � ��������';

COMMENT ON COLUMN scenario_step_params.param_types IS
    '��� ���������';

COMMENT ON COLUMN scenario_step_params.is_mandatory IS
    '�������������� ���������';

CREATE INDEX scenario_step_params_sname_uindex
    ON scenario_step_params (sname);

GRANT ALL ON scenario_step_params TO teco_user;


/*
alarm_category
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.alarm_category', 'table');
    silent_drop ('seq_alarm_category_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_alarm_category_id START WITH 1;

CREATE TABLE alarm_category
(
    id                NUMBER (10, 0) NOT NULL CONSTRAINT pk_alarm_category_id PRIMARY KEY,
    code              VARCHAR2 (32) NOT NULL CONSTRAINT uq_alarm_category_code UNIQUE,
    sname             VARCHAR2 (128) NOT NULL,
    description       VARCHAR2 (512) NOT NULL,
    reaction_level    NUMBER (2, 0) NOT NULL,
    created           DATE DEFAULT SYSDATE,
    creator_code      VARCHAR2 (32) NOT NULL,
    is_deleted        NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fk_alarm_category_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_alarm_category_id
    BEFORE INSERT
    ON alarm_category
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_alarm_category_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE alarm_category IS
    '���������� ��������� ������';

COMMENT ON COLUMN alarm_category.id IS 'PrimaryKey';

COMMENT ON COLUMN alarm_category.sname IS
    '�������� ���������';

COMMENT ON COLUMN alarm_category.description IS
    '�������� ���������';



GRANT ALL ON alarm_category TO teco_user;

/*
alarm_status
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.alarm_type', 'table');
    silent_drop ('seq_alarm_type_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_alarm_type_id START WITH 1;


CREATE TABLE alarm_type
(
    id                     NUMBER (10, 0) NOT NULL CONSTRAINT pk_alarm_type_id PRIMARY KEY,
    code                   VARCHAR2 (32) NOT NULL CONSTRAINT uq_alarm_type_code UNIQUE,
    sname                  VARCHAR2 (128) NOT NULL,
    description            VARCHAR2 (512) NULL,
    external_code          VARCHAR2 (32) NULL,
    alarm_category_code    VARCHAR2 (32) NOT NULL,
    created                DATE DEFAULT SYSDATE,
    creator_code           VARCHAR2 (32) NOT NULL,
    is_deleted             NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT fk_at_alarm_category_code FOREIGN KEY (alarm_category_code)
        REFERENCES alarm_category (code),
    CONSTRAINT fk_alarm_type_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_alarm_type_id
    BEFORE INSERT
    ON alarm_type
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_alarm_type_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE alarm_type IS '���� ������';

COMMENT ON COLUMN alarm_type.id IS 'PrimaryKey';

COMMENT ON COLUMN alarm_type.sname IS '�������� ������';

COMMENT ON COLUMN alarm_type.description IS '�������� ������';
COMMENT ON COLUMN alarm_type.external_code IS
    '��� �� ������� �������';



CREATE INDEX alarm_type_extode_uindex
    ON alarm_type (external_code);


GRANT ALL ON alarm_type TO teco_user;


/*
regions
v 1.0
S.K.
*/


BEGIN
    silent_drop ('teco.regions', 'table');
    silent_drop ('seq_regions_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_regions_id START WITH 1;


CREATE TABLE teco.regions
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_regions_id PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_regions_code UNIQUE,
    sname           VARCHAR (128) NOT NULL,
    description     VARCHAR (512),
    kod_obl         VARCHAR (32),
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR (32) NOT NULL,
    is_deleted      NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fk_regions_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE
);


CREATE OR REPLACE TRIGGER tr_bi_regions_id
    BEFORE INSERT
    ON regions
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_regions_id.NEXTVAL INTO :new.id FROM DUAL;
END;


COMMENT ON TABLE regions IS '���� ��������';

COMMENT ON COLUMN regions.id IS 'PrimaryKey';

COMMENT ON COLUMN regions.sname IS '�������� ��������';

COMMENT ON COLUMN regions.description IS '�������� ';
COMMENT ON COLUMN regions.kod_obl IS '��� �� ��������';


CREATE INDEX regions_extode_uindex
    ON regions (kod_obl);


GRANT ALL ON regions TO teco_user;


/*
dhcp_pool_link
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.dhcp_pool_link', 'table');
    silent_drop ('seq_dhcp_pool_link_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_dhcp_pool_link_id START WITH 1;

CREATE TABLE teco.dhcp_pool_link
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT dhcp_pool_link PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_dhcp_pool_link_code UNIQUE,
    netmask         VARCHAR2 (128) NOT NULL,
    bits            VARCHAR2 (32) NOT NULL,
    gateway         VARCHAR2 (32) NOT NULL,
    lease_time      NUMBER (10, 0) NOT NULL,
    region_code     VARCHAR2 (32) NOT NULL,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT fk_dpl_region_code FOREIGN KEY (region_code)
        REFERENCES regions (code),
    CONSTRAINT fk_dhcp_pool_link_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_dhcp_pool_link_id
    BEFORE INSERT
    ON dhcp_pool_link
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_dhcp_pool_link_id.NEXTVAL INTO :new.id FROM DUAL;
END;


COMMENT ON TABLE dhcp_pool_link IS 'L3 ����  ��������';

COMMENT ON COLUMN dhcp_pool_link.id IS 'PrimaryKey';

COMMENT ON COLUMN dhcp_pool_link.netmask IS '����� ����';

COMMENT ON COLUMN dhcp_pool_link.bits IS
    '���������� ������� ';
COMMENT ON COLUMN dhcp_pool_link.gateway IS '����';
COMMENT ON COLUMN dhcp_pool_link.lease_time IS '����� ������';

CREATE INDEX dhcp_pool_link_netmask_uindex
    ON dhcp_pool_link (netmask);


GRANT ALL ON dhcp_pool_link TO teco_user;

/*
address
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.address', 'table');
    silent_drop ('seq_address_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_address_id START WITH 1;

CREATE TABLE teco.address
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_address_id PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_address_code UNIQUE,
    sname           VARCHAR2 (128) NOT NULL,
    description     VARCHAR2 (512) NULL,
    location        VARCHAR2 (64) NULL,
    postcode        VARCHAR2 (64) NULL,
    region_code     VARCHAR2 (32) NOT NULL,
    town            VARCHAR2 (128) NULL,
    street          VARCHAR2 (128) NULL,
    house           VARCHAR2 (32) NULL,
    apart           VARCHAR2 (32) NULL,
    room            VARCHAR2 (32) NULL,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    changed         DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT fk_address_region_code FOREIGN KEY (region_code)
        REFERENCES regions (code),
    CONSTRAINT fk_address_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_address_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_address_id
    BEFORE INSERT
    ON address
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_address_id.NEXTVAL INTO :new.id FROM DUAL;
END;


GRANT ALL ON address TO teco_user;

/*
device
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.device', 'table');
    silent_drop ('seq_device_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_device_id START WITH 1;

CREATE TABLE device
(
    id                        NUMBER (10, 0) NOT NULL CONSTRAINT pk_device_id PRIMARY KEY,
    code                      VARCHAR2 (32) NOT NULL CONSTRAINT uq_device_code UNIQUE,
    sname                     VARCHAR2 (512) NOT NULL,
    description               VARCHAR2 (512) NULL,
    device_class_code         VARCHAR2 (32) NOT NULL,
    device_type_code          VARCHAR2 (32) NOT NULL,
    interface_code            VARCHAR2 (32) NOT NULL,
    utility_path              VARCHAR2 (1024) NULL,
    port_speed                NUMBER (10, 0) NULL,
    data_bits                 VARCHAR2 (32) NULL,
    is_parity                 NUMBER (1, 0) NULL,
    is_flow_control           NUMBER (1, 0) NULL,
    uart_driver_type          VARCHAR2 (32) NULL,
    is_hw_virtual_port_use    NUMBER (1, 0) NOT NULL,
    is_termo_connected        NUMBER (1, 0) NOT NULL,
    serial_no                 VARCHAR2 (128) NOT NULL,
    inventory_no              VARCHAR2 (128) NOT NULL,
    created                   DATE DEFAULT SYSDATE,
    creator_code              VARCHAR2 (32) NOT NULL,
    changed                   DATE DEFAULT SYSDATE,
    changer_code              VARCHAR2 (32) NOT NULL,
    is_deleted                NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fq_dev_device_class_code FOREIGN KEY (device_class_code)
        REFERENCES teco.device_class (code),
    CONSTRAINT fq_dev_device_type_code FOREIGN KEY (device_type_code)
        REFERENCES teco.device_type (code),
    CONSTRAINT fq_dev_interface_type_code FOREIGN KEY (interface_code)
        REFERENCES teco.interface_type (code),
    CONSTRAINT fk_device_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_device_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_device_id
    BEFORE INSERT
    ON device
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_device_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE device IS
    '��������������� ����������';

COMMENT ON COLUMN device.id IS 'Primary key';

COMMENT ON COLUMN device.sname IS '�������� ����������';

COMMENT ON COLUMN device.description IS
    '�������� ����������';


COMMENT ON COLUMN device.device_class_code IS
    'id ������ ���������';

COMMENT ON COLUMN device.utility_path IS 'vendor utility path';

COMMENT ON COLUMN device.interface_code IS
    '�������� ����� ����������';

COMMENT ON COLUMN device.port_speed IS
    '�������� ����� ���������� ���\���';

COMMENT ON COLUMN device.data_bits IS '�������� ���\���';

COMMENT ON COLUMN device.is_parity IS '��������';

COMMENT ON COLUMN device.is_flow_control IS
    '���������� ������� (��\���)';

COMMENT ON COLUMN device.uart_driver_type IS '��� �������� UART';

COMMENT ON COLUMN device.is_hw_virtual_port_use IS
    '������������ ����������� HW ����';

COMMENT ON COLUMN device.is_termo_connected IS
    '����������� �����������';


COMMENT ON COLUMN device.serial_no IS 'Serial number';
COMMENT ON COLUMN device.inventory_no IS 'Inventory number';



GRANT ALL ON teco.device TO teco_user;

/*
site_group
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.site_group', 'table');
    silent_drop ('seq_site_group_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_site_group_id START WITH 1;

CREATE TABLE teco.site_group
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_site_group_id PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_site_group_code UNIQUE,
    sname           VARCHAR (128) NOT NULL,
    description     VARCHAR (512),
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    changed         DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fk_site_group_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_site_group_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_site_group_id
    BEFORE INSERT
    ON site_group
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_site_group_id.NEXTVAL INTO :new.id FROM DUAL;
END;



GRANT ALL ON site_group TO teco_user;


/*
site
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.site', 'table');
    silent_drop ('seq_site_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_site_id START WITH 1;

CREATE TABLE teco.site
(
    id                 NUMBER (10, 0) NOT NULL CONSTRAINT pk_site_id PRIMARY KEY,
    code               VARCHAR2 (32) NOT NULL CONSTRAINT uq_site_code UNIQUE,
    site_group_code    VARCHAR (32) NOT NULL,
    sname              VARCHAR (128) NOT NULL,
    description        VARCHAR (512) NULL,
    address_code       VARCHAR (32) NOT NULL,
    status_code        VARCHAR (32) NOT NULL,
    alarm_code         VARCHAR (32) NOT NULL,
    port_capacity      INTEGER DEFAULT 8 NOT NULL,
    ip_pool_code       VARCHAR (32) NOT NULL, -- TODO ������ ����� ����������� L3 �����
    created            DATE DEFAULT SYSDATE,
    creator_code       VARCHAR2 (32) NOT NULL,
    changed            DATE DEFAULT SYSDATE,
    changer_code       VARCHAR2 (32) NOT NULL,
    is_deleted         NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fk_s_site_group_code FOREIGN KEY (site_group_code)
        REFERENCES site_group (code) ON DELETE CASCADE,
    CONSTRAINT fk_s_address_code FOREIGN KEY (site_group_code)
        REFERENCES address (code) ON DELETE CASCADE,
    CONSTRAINT site_status_code_fkey FOREIGN KEY (status_code)
        REFERENCES device_status (code),
    CONSTRAINT site_alarm_type_code_fkey FOREIGN KEY (alarm_code)
        REFERENCES alarm_type (code),
    CONSTRAINT fk_site_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_site_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_site_id
    BEFORE INSERT
    ON site
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_site_id.NEXTVAL INTO :new.id FROM DUAL;
END;


COMMENT ON COLUMN site.sname IS 'Site name ';

COMMENT ON COLUMN site.description IS '�������� �����';

COMMENT ON COLUMN site.address_code IS '����� ���������';

COMMENT ON COLUMN site.status_code IS '������ �� �����';

COMMENT ON COLUMN site.alarm_code IS 'id ��������� ������';

COMMENT ON COLUMN site.ip_pool_code IS
    '������ �� ��� ������� DHCP';

COMMENT ON COLUMN site.port_capacity IS
    '���������� ������ �����';



CREATE INDEX site_group_code_uindex
    ON site (site_group_code);

GRANT ALL ON site TO teco_user;

/*
site_item
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.site_item', 'table');
    silent_drop ('seq_site_item_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_site_item_id START WITH 1;

CREATE TABLE site_item
(
    id                 NUMBER (10, 0) NOT NULL CONSTRAINT pk_site_item_id PRIMARY KEY,
    code               VARCHAR2 (32) NOT NULL CONSTRAINT uq_site_item_code UNIQUE,
    site_code          VARCHAR2 (32) NOT NULL,
    port_no            NUMBER (3, 0) NOT NULL,
    device_code        VARCHAR2 (32) NOT NULL,
    interface_code     VARCHAR2 (32) NOT NULL,
    status_code        VARCHAR2 (32) NOT NULL,
    last_alarm_code    NUMBER (3, 0) NOT NULL,
    ip_address         VARCHAR2 (128) NOT NULL,
    ip_pool_code       VARCHAR2 (32) NOT NULL,
    created            DATE DEFAULT SYSDATE,
    creator_code       VARCHAR2 (32) NOT NULL,
    changed            DATE DEFAULT SYSDATE,
    changer_code       VARCHAR2 (32) NOT NULL,
    is_deleted         NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fk_si_site_code FOREIGN KEY (site_code)
        REFERENCES teco.site (code),
    CONSTRAINT fk_si_device_code FOREIGN KEY (device_code)
        REFERENCES teco.device (code),
    CONSTRAINT fk_si_interface_code FOREIGN KEY (interface_code)
        REFERENCES teco.interface_type (code),
    CONSTRAINT fk_si_status_code FOREIGN KEY (status_code)
        REFERENCES teco.device_status (code),
    CONSTRAINT fk_si_ip_pool_code FOREIGN KEY (ip_pool_code)
        REFERENCES teco.dhcp_pool_link (code),
    CONSTRAINT fk_site_item_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_site_item_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_site_item_id
    BEFORE INSERT
    ON site_item
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_site_item_id.NEXTVAL INTO :new.id FROM DUAL;
END;


COMMENT ON TABLE site_item IS '���������� � �����';

COMMENT ON COLUMN site_item.id IS 'Primary Key';

COMMENT ON COLUMN site_item.site_code IS 'Reference to site';

COMMENT ON COLUMN site_item.port_no IS 'Number of port on device';

COMMENT ON COLUMN site_item.device_code IS
    '���������� � �����';

COMMENT ON COLUMN site_item.status_code IS
    '�� ������� ����������';

COMMENT ON COLUMN site_item.last_alarm_code IS
    '��������� ��� �������';



CREATE UNIQUE INDEX site_item_siteid_portno
    ON site_item (status_code, port_no);

GRANT ALL ON teco.site_item TO teco_user;

COMMIT;
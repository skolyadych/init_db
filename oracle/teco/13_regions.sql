/* Formatted on 24-���-2019 08:40:52 (QP5 v5.336) */
/*
regions
v 1.0
S.K.
*/


BEGIN
    silent_drop ('teco.regions', 'table');
    silent_drop ('seq_regions_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_regions_id START WITH 1;


CREATE TABLE teco.regions
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_regions_id PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_regions_code UNIQUE,
    sname           VARCHAR (128) NOT NULL,
    description     VARCHAR (512),
    kod_obl         VARCHAR (32),
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR (32) NOT NULL,
    is_deleted      NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fk_regions_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE
);


CREATE OR REPLACE TRIGGER tr_bi_regions_id
    BEFORE INSERT
    ON regions
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_regions_id.NEXTVAL INTO :new.id FROM DUAL;
END;


COMMENT ON TABLE regions IS '���� ��������';

COMMENT ON COLUMN regions.id IS 'PrimaryKey';

COMMENT ON COLUMN regions.sname IS '�������� ��������';

COMMENT ON COLUMN regions.description IS '�������� ';
COMMENT ON COLUMN regions.kod_obl IS '��� �� ��������';


CREATE INDEX regions_extode_uindex
    ON regions (kod_obl);


GRANT ALL ON regions TO teco_user;
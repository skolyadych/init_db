
/*
device_configuration
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.device_config', 'table');
    silent_drop ('seq_device_config_id', 'sequence');

    COMMIT;
END;

CREATE SEQUENCE seq_device_config_id START WITH 1;



CREATE TABLE device_config
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_device_config_pk PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_device_config_code UNIQUE,
    sname           VARCHAR2 (128) NOT NULL,
    description     VARCHAR2 (512),
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    changed         DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fk_device_config_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_device_config_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);


CREATE OR REPLACE TRIGGER tr_bi_device_config_id
    BEFORE INSERT
    ON device_config
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_device_config_id.NEXTVAL INTO :new.id FROM DUAL;
END;


COMMENT ON TABLE device_config IS
    '������������ ����� ���������';

COMMENT ON COLUMN device_config.id IS 'Primary Key';

COMMENT ON COLUMN device_config.sname IS 'ShortName';

COMMENT ON COLUMN device_config.description IS
    '�������� ������������';


GRANT ALL ON teco.device_config TO teco_user;
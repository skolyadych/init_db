/* Formatted on 24-���-2019 09:34:26 (QP5 v5.336) */
/*
site_group
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.site_group', 'table');
    silent_drop ('seq_site_group_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_site_group_id START WITH 1;

CREATE TABLE teco.site_group
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_site_group_id PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_site_group_code UNIQUE,
    sname           VARCHAR (128) NOT NULL,
    description     VARCHAR (512),
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    changed         DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fk_site_group_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_site_group_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_site_group_id
    BEFORE INSERT
    ON site_group
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_site_group_id.NEXTVAL INTO :new.id FROM DUAL;
END;



GRANT ALL ON site_group TO teco_user;
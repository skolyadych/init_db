/* Formatted on 24-���-2019 09:17:23 (QP5 v5.336) */
/*
address
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.address', 'table');
    silent_drop ('seq_address_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_address_id START WITH 1;

CREATE TABLE teco.address
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_address_id PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_address_code UNIQUE,
    sname           VARCHAR2 (128) NOT NULL,
    description     VARCHAR2 (512) NULL,
    location        VARCHAR2 (64) NULL,
    postcode        VARCHAR2 (64) NULL,
    region_code     VARCHAR2 (32) NOT NULL,
    town            VARCHAR2 (128) NULL,
    street          VARCHAR2 (128) NULL,
    house           VARCHAR2 (32) NULL,
    apart           VARCHAR2 (32) NULL,
    room            VARCHAR2 (32) NULL,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    changed         DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT fk_address_region_code FOREIGN KEY (region_code)
        REFERENCES regions (code),
    CONSTRAINT fk_address_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_address_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_address_id
    BEFORE INSERT
    ON address
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_address_id.NEXTVAL INTO :new.id FROM DUAL;
END;


GRANT ALL ON address TO teco_user;
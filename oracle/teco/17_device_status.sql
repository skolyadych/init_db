/* Formatted on 24-���-2019 09:28:59 (QP5 v5.336) */
/*
device_status
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.device_status', 'table');
    silent_drop ('seq_device_status_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_device_status_id START WITH 1;

CREATE TABLE device_status
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_device_status_id PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_device_status_code UNIQUE,
    sname           VARCHAR2 (128) NOT NULL,
    description     VARCHAR2 (512) NULL,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT fk_device_status_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_device_status_id
    BEFORE INSERT
    ON device_status
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_device_status_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE device_status IS
    '���� �������� ���������';

COMMENT ON COLUMN device_status.id IS 'PrimaryKey';

COMMENT ON COLUMN device_status.sname IS '�������� �������';

COMMENT ON COLUMN device_status.description IS
    '�������� �������';



GRANT ALL ON device_status TO teco_user;
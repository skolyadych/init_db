/* Formatted on 24-���-2019 09:53:33 (QP5 v5.336) */
/*
site
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.site', 'table');
    silent_drop ('seq_site_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_site_id START WITH 1;

CREATE TABLE teco.site
(
    id                 NUMBER (10, 0) NOT NULL CONSTRAINT pk_site_id PRIMARY KEY,
    code               VARCHAR2 (32) NOT NULL CONSTRAINT uq_site_code UNIQUE,
    site_group_code    VARCHAR (32) NOT NULL,
    sname              VARCHAR (128) NOT NULL,
    description        VARCHAR (512) NULL,
    address_code       VARCHAR (32) NOT NULL,
    status_code        VARCHAR (32) NOT NULL,
    alarm_code         VARCHAR (32) NOT NULL,
    port_capacity      INTEGER DEFAULT 8 NOT NULL,
    ip_pool_code       VARCHAR (32) NOT NULL, -- TODO ������ ����� ����������� L3 �����
    created            DATE DEFAULT SYSDATE,
    creator_code       VARCHAR2 (32) NOT NULL,
    changed            DATE DEFAULT SYSDATE,
    changer_code       VARCHAR2 (32) NOT NULL,
    is_deleted         NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fk_s_site_group_code FOREIGN KEY (site_group_code)
        REFERENCES site_group (code) ON DELETE CASCADE,
    CONSTRAINT fk_s_address_code FOREIGN KEY (site_group_code)
        REFERENCES address (code) ON DELETE CASCADE,
    CONSTRAINT site_status_code_fkey FOREIGN KEY (status_code)
        REFERENCES device_status (code),
    CONSTRAINT site_alarm_type_code_fkey FOREIGN KEY (alarm_code)
        REFERENCES alarm_type (code),
    CONSTRAINT fk_site_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_site_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_site_id
    BEFORE INSERT
    ON site
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_site_id.NEXTVAL INTO :new.id FROM DUAL;
END;


COMMENT ON COLUMN site.sname IS 'Site name ';

COMMENT ON COLUMN site.description IS '�������� �����';

COMMENT ON COLUMN site.address_code IS '����� ���������';

COMMENT ON COLUMN site.status_code IS '������ �� �����';

COMMENT ON COLUMN site.alarm_code IS 'id ��������� ������';

COMMENT ON COLUMN site.ip_pool_code IS
    '������ �� ��� ������� DHCP';

COMMENT ON COLUMN site.port_capacity IS
    '���������� ������ �����';



CREATE INDEX site_group_code_uindex
    ON site (site_group_code);

GRANT ALL ON site TO teco_user;
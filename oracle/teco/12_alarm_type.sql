/* Formatted on 24-���-2019 08:29:36 (QP5 v5.336) */
/*
alarm_status
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.alarm_type', 'table');
    silent_drop ('seq_alarm_type_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_alarm_type_id START WITH 1;


CREATE TABLE alarm_type
(
    id                     NUMBER (10, 0) NOT NULL CONSTRAINT pk_alarm_type_id PRIMARY KEY,
    code                   VARCHAR2 (32) NOT NULL CONSTRAINT uq_alarm_type_code UNIQUE,
    sname                  VARCHAR2 (128) NOT NULL,
    description            VARCHAR2 (512) NULL,
    external_code          VARCHAR2 (32) NULL,
    alarm_category_code    VARCHAR2 (32) NOT NULL,
    created                DATE DEFAULT SYSDATE,
    creator_code           VARCHAR2 (32) NOT NULL,
    is_deleted             NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT fk_at_alarm_category_code FOREIGN KEY (alarm_category_code)
        REFERENCES alarm_category (code),
    CONSTRAINT fk_alarm_type_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_alarm_type_id
    BEFORE INSERT
    ON alarm_type
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_alarm_type_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE alarm_type IS '���� ������';

COMMENT ON COLUMN alarm_type.id IS 'PrimaryKey';

COMMENT ON COLUMN alarm_type.sname IS '�������� ������';

COMMENT ON COLUMN alarm_type.description IS '�������� ������';
COMMENT ON COLUMN alarm_type.external_code IS
    '��� �� ������� �������';



CREATE INDEX alarm_type_extode_uindex
    ON alarm_type (external_code);


GRANT ALL ON alarm_type TO teco_user;
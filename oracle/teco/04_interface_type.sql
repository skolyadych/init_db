/* Formatted on 23-���-2019 21:36:07 (QP5 v5.336) */
/*
interface_type
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.interface_type', 'table');
    silent_drop ('seq_interface_type_id', 'sequence');

    COMMIT;
END;

CREATE SEQUENCE seq_interface_type_id START WITH 1;


CREATE TABLE interface_type
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_interface_type_pk PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_interface_type_code UNIQUE,
    sname           VARCHAR2 (128) NOT NULL,
    description     VARCHAR2 (512) NULL,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT fk_interface_type_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE
);


CREATE OR REPLACE TRIGGER tr_bi_interface_type_id
    BEFORE INSERT
    ON interface_type
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_interface_type_id.NEXTVAL INTO :new.id FROM DUAL;
END;


COMMENT ON TABLE interface_type IS
    '���� ����������� ���������';

COMMENT ON COLUMN interface_type.id IS 'PrimaryKey';

COMMENT ON COLUMN interface_type.sname IS
    '�������� �����������';

COMMENT ON COLUMN interface_type.description IS
    '�������� ���������';


GRANT ALL ON interface_type TO teco_user;
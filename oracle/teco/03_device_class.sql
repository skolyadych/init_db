/* Formatted on 23-���-2019 21:24:17 (QP5 v5.336) */
/*
device_class
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.device_class', 'table');
    silent_drop ('seq_device_class_id', 'sequence');
    silent_drop ('seq_device_class_order_no', 'sequence');
    COMMIT;
END;

CREATE sequence seq_device_class_id start with 1;
CREATE sequence seq_device_class_order_no start with 1;

CREATE TABLE teco.device_class
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT pk_device_class PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL CONSTRAINT uq_device_class_code UNIQUE,
    sname           VARCHAR2 (128) NOT NULL,
    description     VARCHAR2 (512) NULL,
    order_no        NUMBER (10, 0)
                       NOT NULL
                       CONSTRAINT uq_device_class_order UNIQUE,
    request_rate    NUMBER (15, 0) DEFAULT 0,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    chaged          DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT fk_device_class_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_device_class_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);


CREATE OR REPLACE TRIGGER tr_bi_device_class_id
    BEFORE INSERT
    ON device_class
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_device_class_id.NEXTVAL INTO :new.id FROM DUAL;
END;

CREATE OR REPLACE TRIGGER tr_bi_device_class_order_no
    BEFORE INSERT
    ON device_class
    FOR EACH ROW
    WHEN (new.order_no IS NULL)
BEGIN
    SELECT seq_device_class_order_no.NEXTVAL INTO :new.order_no FROM DUAL;
END;



COMMENT ON TABLE device_class IS '������ ���������';

COMMENT ON COLUMN device_class.id IS 'Primary key';

COMMENT ON COLUMN device_class.sname IS
    '�������� ��� ��� ������ ����';

COMMENT ON COLUMN device_class.description IS
    '�������� ������ ��������� (��� ����������� ���������';

COMMENT ON COLUMN device_class.order_no IS
    '���������� ����� (��� ����������)';

COMMENT ON COLUMN device_class.request_rate IS
    '������� ������ �� ��������� � �������� (0-�� ����������)';


GRANT ALL ON teco.users TO teco_user;
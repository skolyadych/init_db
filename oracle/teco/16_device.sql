/* Formatted on 24-���-2019 09:17:58 (QP5 v5.336) */
/*
device
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.device', 'table');
    silent_drop ('seq_device_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_device_id START WITH 1;

CREATE TABLE device
(
    id                        NUMBER (10, 0) NOT NULL CONSTRAINT pk_device_id PRIMARY KEY,
    code                      VARCHAR2 (32) NOT NULL CONSTRAINT uq_device_code UNIQUE,
    sname                     VARCHAR2 (512) NOT NULL,
    description               VARCHAR2 (512) NULL,
    device_class_code         VARCHAR2 (32) NOT NULL,
    device_type_code          VARCHAR2 (32) NOT NULL,
    interface_code            VARCHAR2 (32) NOT NULL,
    utility_path              VARCHAR2 (1024) NULL,
    port_speed                NUMBER (10, 0) NULL,
    data_bits                 VARCHAR2 (32) NULL,
    is_parity                 NUMBER (1, 0) NULL,
    is_flow_control           NUMBER (1, 0) NULL,
    uart_driver_type          VARCHAR2 (32) NULL,
    is_hw_virtual_port_use    NUMBER (1, 0) NOT NULL,
    is_termo_connected        NUMBER (1, 0) NOT NULL,
    serial_no                 VARCHAR2 (128) NOT NULL,
    inventory_no              VARCHAR2 (128) NOT NULL,
    created                   DATE DEFAULT SYSDATE,
    creator_code              VARCHAR2 (32) NOT NULL,
    changed                   DATE DEFAULT SYSDATE,
    changer_code              VARCHAR2 (32) NOT NULL,
    is_deleted                NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fq_dev_device_class_code FOREIGN KEY (device_class_code)
        REFERENCES teco.device_class (code),
    CONSTRAINT fq_dev_device_type_code FOREIGN KEY (device_type_code)
        REFERENCES teco.device_type (code),
    CONSTRAINT fq_dev_interface_type_code FOREIGN KEY (interface_code)
        REFERENCES teco.interface_type (code),
    CONSTRAINT fk_device_code_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_device_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_device_id
    BEFORE INSERT
    ON device
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_device_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE device IS
    '��������������� ����������';

COMMENT ON COLUMN device.id IS 'Primary key';

COMMENT ON COLUMN device.sname IS '�������� ����������';

COMMENT ON COLUMN device.description IS
    '�������� ����������';


COMMENT ON COLUMN device.device_class_code IS
    'id ������ ���������';

COMMENT ON COLUMN device.utility_path IS 'vendor utility path';

COMMENT ON COLUMN device.interface_code IS
    '�������� ����� ����������';

COMMENT ON COLUMN device.port_speed IS
    '�������� ����� ���������� ���\���';

COMMENT ON COLUMN device.data_bits IS '�������� ���\���';

COMMENT ON COLUMN device.is_parity IS '��������';

COMMENT ON COLUMN device.is_flow_control IS
    '���������� ������� (��\���)';

COMMENT ON COLUMN device.uart_driver_type IS '��� �������� UART';

COMMENT ON COLUMN device.is_hw_virtual_port_use IS
    '������������ ����������� HW ����';

COMMENT ON COLUMN device.is_termo_connected IS
    '����������� �����������';


COMMENT ON COLUMN device.serial_no IS 'Serial number';
COMMENT ON COLUMN device.inventory_no IS 'Inventory number';



GRANT ALL ON teco.device TO teco_user;
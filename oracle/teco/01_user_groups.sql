/* Formatted on 23-���-2019 21:06:34 (QP5 v5.336) */
/*
Ueser Roles
v 1.0
*/


BEGIN
    silent_drop ('teco.user_roles', 'table');
    silent_drop ('seq_user_roles_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_user_roles_id START WITH 1;

CREATE TABLE user_roles
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT user_roles_pk PRIMARY KEY,
    code            VARCHAR2 (32) NOT NULL,
    role_name       VARCHAR2 (128) NOT NULL,
    domain_group    VARCHAR2 (512) NULL,
    description     VARCHAR2 (512) NULL,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NOT NULL,
    changed         DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NOT NULL,
    is_deleted      NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT uq_user_roles_code UNIQUE (code)
);

CREATE OR REPLACE TRIGGER tr_bi_user_roles_id
    BEFORE INSERT
    ON user_roles
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_user_roles_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE user_roles IS '������ ������������';

COMMENT ON COLUMN user_roles.id IS 'Primary key';

COMMENT ON COLUMN user_roles.role_name IS 'Role name';

COMMENT ON COLUMN user_roles.domain_group IS 'ActiveDirectory group';

COMMENT ON COLUMN user_roles.description IS 'RoleDescription';


GRANT ALL ON teco.user_roles TO teco_user;


INSERT INTO teco.user_roles (code,
                             role_name,
                             domain_group,
                             description,
                             creator_code,
                             changer_code)
VALUES ('owner',
        'teco_owner',
        'KS_Teco_owners',
        'Teco Developpers',
        'owner',
        'owner');

COMMIT;

SELECT * FROM teco.user_roles
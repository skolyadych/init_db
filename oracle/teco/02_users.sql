/* Formatted on 23-���-2019 21:11:25 (QP5 v5.336) */
/*
Uesers
v 1.0
*/


BEGIN
    silent_drop ('teco.users', 'table');
    silent_drop ('seq_users_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_users_id START WITH 1;

CREATE TABLE teco.users
(
    id              NUMBER (10, 0) NOT NULL CONSTRAINT users_pk PRIMARY KEY, --primary key
    login           VARCHAR2 (128) NOT NULL,
    first_name      VARCHAR2 (128) NOT NULL,
    last_name       VARCHAR2 (128) NOT NULL,
    middle_name     VARCHAR2 (128) NULL,
    group_code      VARCHAR2 (32) NOT NULL,
    position        VARCHAR2 (512) NULL,
    created         DATE DEFAULT SYSDATE,
    creator_code    VARCHAR2 (32) NULL,
    changed         DATE DEFAULT SYSDATE,
    changer_code    VARCHAR2 (32) NULL,
    is_deleted      NUMBER (1, 0) DEFAULT 0,
    CONSTRAINT uq_users_name UNIQUE (login),
    CONSTRAINT fk_user_roles_code FOREIGN KEY (group_code)
        REFERENCES user_roles (code) ON DELETE CASCADE
);


CREATE OR REPLACE TRIGGER tr_bi_users_id
    BEFORE INSERT
    ON users
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_users_id.NEXTVAL INTO :new.id FROM DUAL;
END;



GRANT ALL ON teco.users TO teco_user;

INSERT INTO users (login,
                   first_name,
                   last_name,
                   middle_name,
                   position,
                   group_code)
VALUES ('admin',
        'Sergey',
        'Kolyadich',
        'Arkadievich',
        'Developper',
        'owner');

INSERT INTO users (login,
                   first_name,
                   last_name,
                   middle_name,
                   position,
                   group_code)
VALUES ('teco',
        'creator',
        'teco',
        'teco',
        'dbowner',
        'owner');


COMMIT;
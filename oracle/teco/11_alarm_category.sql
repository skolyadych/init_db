/* Formatted on 24-���-2019 08:16:49 (QP5 v5.336) */
/*
alarm_category
v 1.0
S.K.
*/

BEGIN
    silent_drop ('teco.alarm_category', 'table');
    silent_drop ('seq_alarm_category_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_alarm_category_id START WITH 1;

CREATE TABLE alarm_category
(
    id                NUMBER (10, 0) NOT NULL CONSTRAINT pk_alarm_category_id PRIMARY KEY,
    code              VARCHAR2 (32) NOT NULL CONSTRAINT uq_alarm_category_code UNIQUE,
    sname             VARCHAR2 (128) NOT NULL,
    description       VARCHAR2 (512) NOT NULL,
    reaction_level    NUMBER (2, 0) NOT NULL,
    created           DATE DEFAULT SYSDATE,
    creator_code      VARCHAR2 (32) NOT NULL,
    is_deleted        NUMERIC (1, 0) DEFAULT 0,
    CONSTRAINT fk_alarm_category_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER tr_bi_alarm_category_id
    BEFORE INSERT
    ON alarm_category
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_alarm_category_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE alarm_category IS
    '���������� ��������� ������';

COMMENT ON COLUMN alarm_category.id IS 'PrimaryKey';

COMMENT ON COLUMN alarm_category.sname IS
    '�������� ���������';

COMMENT ON COLUMN alarm_category.description IS
    '�������� ���������';



GRANT ALL ON alarm_category TO teco_user;
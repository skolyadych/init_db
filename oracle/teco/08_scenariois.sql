/* Formatted on 23-���-2019 23:03:54 (QP5 v5.336) */
/*
commands
v 1.0
S.K.
*/


BEGIN
    silent_drop ('teco.scenarios', 'table');
    silent_drop ('seq_scenarios_id', 'sequence');
    COMMIT;
END;
/

CREATE SEQUENCE seq_scenarios_id START WITH 1;

CREATE TABLE teco.scenarios
(
    id                  NUMBER (10, 0) NOT NULL CONSTRAINT pk_scenarios_id PRIMARY KEY,
    code                VARCHAR2 (32) NOT NULL CONSTRAINT uq_scenarios_code UNIQUE,
    command_code        VARCHAR2 (32) NOT NULL,
    sname               VARCHAR2 (128) NOT NULL,
    step                NUMBER (3, 0) NOT NULL,
    request_pattern     VARCHAR2 (2048) NULL,
    responce_pattern    VARCHAR2 (2048) NULL,
    description         VARCHAR2 (512) NULL,
    created             DATE DEFAULT SYSDATE,
    creator_code        VARCHAR2 (32) NOT NULL,
    changed             DATE DEFAULT SYSDATE,
    changer_code        VARCHAR2 (32) NULL,
    is_deleted          NUMBER (1, 0) DEFAULT 0 NOT NULL,
    CONSTRAINT uq_scenarios_order_com UNIQUE (command_code, step),
    CONSTRAINT fk_sc_command_code FOREIGN KEY (command_code)
        REFERENCES teco.commands (code),
    CONSTRAINT fk_scenarios_creator FOREIGN KEY (creator_code)
        REFERENCES users (login) ON DELETE CASCADE,
    CONSTRAINT fk_scenarios_changer FOREIGN KEY (changer_code)
        REFERENCES users (login) ON DELETE CASCADE
);


CREATE OR REPLACE TRIGGER tr_bi_scenarios_id
    BEFORE INSERT
    ON scenarios
    FOR EACH ROW
    WHEN (new.id IS NULL)
BEGIN
    SELECT seq_scenarios_id.NEXTVAL INTO :new.id FROM DUAL;
END;

COMMENT ON TABLE scenarios IS '�������� �������';

COMMENT ON COLUMN scenarios.id IS 'Primary key';

COMMENT ON COLUMN scenarios.command_code IS 'Reference to commands';

COMMENT ON COLUMN scenarios.sname IS
    '�������� ���� ��������';

COMMENT ON COLUMN scenarios.step IS '��� ��������';

COMMENT ON COLUMN scenarios.request_pattern IS 'Request pattern';

COMMENT ON COLUMN scenarios.responce_pattern IS 'Responce pattern';

COMMENT ON COLUMN scenarios.description IS '�������� ����';


GRANT ALL ON teco.scenarios TO teco_user;
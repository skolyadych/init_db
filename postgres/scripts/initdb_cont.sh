#!/bin/sh

export TECO_SCHEMA_NAME=tecomon
export TECO_DATAFILES_LOCATION=/pgdata
export TECO_OWNER=teco
export DB_OWNER=postres                                                            
export TECO_PASSWORD=c67
export TECO_TABLESPACE=TECOMON_DATA
export TECO_DBNAME=tecomondb
export TECO_ROLE=teco_users

psql -X -h localhost -U ${DB_OWNER} -f teco_createdb.sql tecomondb;
psql -X -h localhost -U ${TECO_OWNER} -f addSchema.sql tecomondb;
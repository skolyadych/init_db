/*
site
v 1.0
S.K.
*/

DROP TABLE IF EXISTS teco.site CASCADE;

CREATE TABLE teco.site
(
    id              SERIAL                             NOT NULL
        CONSTRAINT site_pk
            PRIMARY KEY,
    code            VARCHAR(32)                        NOT NULL UNIQUE,
    site_group_code VARCHAR(32)                        NOT NULL UNIQUE REFERENCES site_group (code),
    sname           VARCHAR(128)                       NOT NULL,
    description     VARCHAR(512),
    address_code    VARCHAR(32)                        NOT NULL UNIQUE REFERENCES address (code),
    status_code     VARCHAR(32)                        NOT NULL
        CONSTRAINT site_status_code_fkey
            REFERENCES device_status (code),
    arlarm_code     VARCHAR(32)                        NOT NULL
        CONSTRAINT site_arlarm_code_fkey
            REFERENCES alarm_type (code),
    port_capacity   INTEGER                  DEFAULT 8 NOT NULL,
    ip_pool_code    VARCHAR(32)                        NOT NULL UNIQUE,
    created         TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code    VARCHAR(32)                        NOT NULL REFERENCES users (login),
    chaged          TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    changer_code    VARCHAR(32)                        NOT NULL REFERENCES users (login),
    is_deleted      NUMERIC(1, 0)            DEFAULT 0
);

COMMENT ON COLUMN site.sname IS 'Site name ';

COMMENT ON COLUMN site.description IS 'Описание шкафа';

COMMENT ON COLUMN site.address_code IS 'Адрес установки';

COMMENT ON COLUMN site.status_code IS 'Статус по шкафу';

COMMENT ON COLUMN site.arlarm_code IS 'id последней аварии';

COMMENT ON COLUMN site.ip_pool_code IS 'Ссылка на пул адресов DHCP';

COMMENT ON COLUMN site.port_capacity IS 'Количество портов сайта';

ALTER TABLE site
    OWNER TO teco;

CREATE UNIQUE INDEX site_code_uindex
    ON site (code);

CREATE INDEX site_group_code_uindex
    ON site (site_group_code);

GRANT ALL ON site TO teco_users;
/*
device_types
v 1.0
S.K.
*/

DROP TABLE IF EXISTS teco.interface_type CASCADE;

CREATE TABLE interface_type
(
    id          SERIAL
        CONSTRAINT interface_type_pk
            PRIMARY KEY,
    code        VARCHAR(32) NOT NULL,
    sname       VARCHAR(128) NOT NULL,
    description VARCHAR(512),
    created     TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code VARCHAR(32)  NOT NULL REFERENCES users (login),
    is_deleted  NUMERIC(1, 0)            DEFAULT 0
);

COMMENT ON TABLE interface_type IS 'Типы интерфейсов устройств';

COMMENT ON COLUMN interface_type.id IS 'PrimaryKey';

COMMENT ON COLUMN interface_type.sname IS 'Название интерфейсов';

COMMENT ON COLUMN interface_type.description IS 'Описание параметра';

CREATE UNIQUE INDEX interface_type_code_uindex
    ON interface_type (code);

GRANT ALL ON interface_type TO teco_users;
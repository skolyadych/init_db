DO
$$
    DECLARE
        usersrow           teco.users%ROWTYPE;
        device_clsses_rec  RECORD;
        interface_type_rec RECORD;
        param_type_rec     RECORD;
        device_config_rec  RECORD;
        commands_rec       RECORD;
    BEGIN


        SELECT u.*
        INTO usersrow
        FROM teco.users u
        WHERE u.login = 'admin';
        RAISE NOTICE 'test user id = (%) ', usersrow.login;

        --  device_class
        TRUNCATE TABLE teco.device_class CASCADE;

        INSERT INTO device_class(code, sname, description, request_rate, order_no, creator_code, changer_code)
        VALUES ('counter', 'Лічильники', 'Лічильник електроенергії', 1800, 1, usersrow.login, usersrow.login);

        INSERT INTO device_class(code, sname, description, request_rate, order_no, creator_code, changer_code)
        VALUES ('clima', 'кондиціонери', 'Кондиціонери', 1800, 2, usersrow.login, usersrow.login);

        INSERT INTO device_class(code, sname, description, request_rate, order_no, creator_code, changer_code)
        VALUES ('generator', 'Генератори', 'Дизель-генератори', 1800, 3, usersrow.login, usersrow.login);

        INSERT INTO device_class(code, sname, description, request_rate, order_no, creator_code, changer_code)
        VALUES ('vcounter', 'Лічиники (віртуальні)', 'Віртуальні лічильнники (дані імпортуються)', 1800, 4,
                usersrow.login,
                usersrow.login);

        SELECT code                         AS counter_class_id,
               (SELECT code
                FROM device_class d
                WHERE d.code = 'clima')     AS climat_class_id,
               (SELECT code
                FROM device_class d
                WHERE d.code = 'generator') AS generators_class_id
        INTO device_clsses_rec
        FROM device_class d
        WHERE d.code = 'counter';

        TRUNCATE TABLE teco.interface_type CASCADE;

        INSERT INTO teco.interface_type(code, sname, description, creator_code)
        VALUES ('eth', 'Ethernet', 'Ethernet RJ-45', usersrow.login);
        INSERT INTO teco.interface_type(code, sname, description, creator_code)
        VALUES ('rs232', 'RS-232', 'Порт RS-232', usersrow.login);
        INSERT INTO teco.interface_type(code, sname, description, creator_code)
        VALUES ('rs485', 'RS-485', 'Порт RS-485', usersrow.login);
        INSERT INTO teco.interface_type(code, sname, description, creator_code)
        VALUES ('vport', 'VirtualPort', 'Virtual port', usersrow.login);

        SELECT t.code                                                                   AS rs232id,
               (SELECT t.code FROM teco.interface_type t WHERE lower(t.code) = 'eth')   AS ethernetid,
               (SELECT t.code FROM teco.interface_type t WHERE lower(t.code) = 'rs485') AS rs485id
        INTO interface_type_rec
        FROM teco.interface_type t
        WHERE lower(t.code) = 'rs232';


        TRUNCATE TABLE teco.param_types CASCADE;

        INSERT INTO teco.param_types(code, sname, description, creator_code)
        VALUES ('str', 'String', 'Строка', usersrow.login);
        INSERT INTO teco.param_types(code, sname, description, creator_code)
        VALUES ('int', 'Integer', 'Целое число', usersrow.login);
        INSERT INTO teco.param_types(code, sname, description, creator_code)
        VALUES ('bit', 'Bit', 'Бит', usersrow.login);
        INSERT INTO teco.param_types(code, sname, description, creator_code)
        VALUES ('dec', 'Decimal', 'Число с плавающей точкой', usersrow.login);

        SELECT code                                                          AS stringid,
               (SELECT code FROM teco.param_types WHERE lower(code) = 'int') AS intid,
               (SELECT code FROM teco.param_types WHERE lower(code) = 'bit') AS bitid,
               (SELECT code FROM teco.param_types WHERE lower(code) = 'dec') AS decimalid
        INTO param_type_rec
        FROM teco.param_types
        WHERE lower(code) = 'str';

        TRUNCATE TABLE teco.device_config CASCADE;

        INSERT INTO teco.device_config(code, sname, creator_code, changer_code)
        VALUES ('counters_base', 'Базова конфігурація (лічильнки)', usersrow.login, usersrow.login);
        INSERT INTO teco.device_config(code, sname, creator_code, changer_code)
        VALUES ('generators_base', 'Базова конфігурація (генератори)', usersrow.login, usersrow.login);
        INSERT INTO teco.device_config(code, sname, creator_code, changer_code)
        VALUES ('climat_base', 'Базова конфігурація клімат', usersrow.login, usersrow.login);
        INSERT INTO teco.device_config(code, sname, creator_code, changer_code)
        VALUES ('default', 'Загальна конфігурація', usersrow.login, usersrow.login);

        SELECT code                                                                        AS counters_base,
               (SELECT code FROM teco.device_config WHERE lower(code) = 'generators_base') AS generators_base,
               (SELECT code FROM teco.device_config WHERE lower(code) = 'climat_base')     AS climat_base,
               (SELECT code FROM teco.device_config WHERE lower(code) = 'default')         AS default_base
        INTO device_config_rec
        FROM teco.device_config
        WHERE lower(code) = 'counters_base';

        TRUNCATE TABLE device_type CASCADE;

        INSERT INTO teco.device_type(code, model, description, vendor, device_class_code, utility_path, interface_code,
                                     port_speed, data_bits, is_parity, is_flow_control, uart_driver_type,
                                     is_hw_virtual_port_use, is_termo_connected, configuration_code, creator_code,
                                     changer_code)
        VALUES ('bza10821', 'BZA 108 51 (Actura 48701 HOD)', 'Лічильник Actura 48701 HOD)', 'Actura inc',
                device_clsses_rec.counter_class_id, 'usr/util/win64', interface_type_rec.rs485id, 9600, '4,5,6', 1, 1,
                'transpanent', 1, 0, device_config_rec.counters_base, usersrow.login, usersrow.login);

        INSERT INTO teco.device_type(code, model, description, vendor, device_class_code, utility_path, interface_code,
                                     port_speed, data_bits, is_parity, is_flow_control, uart_driver_type,
                                     is_hw_virtual_port_use, is_termo_connected, configuration_code, creator_code,
                                     changer_code)
        VALUES ('tecoair', 'Aircool Teco (Ethernet)', 'Aircool with ethernet control', 'Teco inc',
                device_clsses_rec.climat_class_id, 'usr/util/win64', interface_type_rec.ethernetid, 0, NULL, 0, 0,
                'transpanent', 1, 1, device_config_rec.climat_base, usersrow.login, usersrow.login);
        INSERT INTO teco.device_type(code, model, description, vendor, device_class_code, utility_path, interface_code,
                                     port_speed, data_bits, is_parity, is_flow_control, uart_driver_type,
                                     is_hw_virtual_port_use, is_termo_connected, configuration_code, creator_code,
                                     changer_code)
        VALUES ('sitronic', 'Aircool Sitronic (Ethernet', 'Aircool Sitronic  with ethernet control', 'Air cool',
                device_clsses_rec.climat_class_id, 'usr/util/win64', interface_type_rec.ethernetid, 0, NULL, 0, 0,
                'transpanent', 1, 1, device_config_rec.climat_base, usersrow.login, usersrow.login);

        INSERT INTO teco.device_type(code, model, description, vendor, device_class_code, utility_path, interface_code,
                                     port_speed, data_bits, is_parity, is_flow_control, uart_driver_type,
                                     is_hw_virtual_port_use, is_termo_connected, configuration_code, creator_code,
                                     changer_code)
        VALUES ('dse', 'DSE generator RS-485', 'Disel Solution Enterprise generator', 'Disel Solution Enterprise ',
                device_clsses_rec.generators_class_id, 'usr/util/win64', interface_type_rec.rs485id, 0, NULL, 1, 0,
                'cl', 1, 1, device_config_rec.generators_base, usersrow.login, usersrow.login);


        TRUNCATE TABLE teco.commands CASCADE;

        INSERT INTO teco.commands(code, configuration_code, direction, order_no, sname, description, creator_code,
                                  changer_code)
        VALUES ('init', device_config_rec.default_base, 1, 1, 'INIT', 'Проверка доступности устройства', usersrow.login,
                usersrow.login);

        INSERT INTO teco.commands(code, configuration_code, direction, order_no, sname, description, creator_code,
                                  changer_code)
        VALUES ('counters', device_config_rec.counters_base, 1, 1, 'GET COUNTERS', 'Получение показателей счетчика',
                usersrow.login,
                usersrow.login);

        INSERT INTO teco.commands(code, configuration_code, direction, order_no, sname, description, creator_code,
                                  changer_code)
        VALUES ('temp', device_config_rec.climat_base, 1, 1, 'TEMPERATURE', 'Получение текущей температуры',
                usersrow.login,
                usersrow.login);

        INSERT INTO teco.commands(code, configuration_code, direction, order_no, sname, description, creator_code,
                                  changer_code)
        VALUES ('fuel', device_config_rec.generators_base, 1, 1, 'FUEL', 'Проверка уровня топлива в генераторе',
                usersrow.login,
                usersrow.login);

        SELECT code                                                            AS init_command,
               (SELECT code FROM teco.commands WHERE lower(code) = 'counters') AS counters,
               (SELECT code FROM teco.commands WHERE lower(code) = 'temp')     AS temp,
               (SELECT code FROM teco.commands WHERE lower(code) = 'fuel')     AS fuel
        INTO commands_rec
        FROM teco.commands
        WHERE lower(code) = 'init';

        TRUNCATE TABLE scenarios CASCADE;

        INSERT INTO scenarios (command_code, code, sname, step, request_pattern, responce_pattern, description,
                               creator_code, changer_code)
        VALUES (commands_rec.init_command, 'reachaddr', 'reach adress', 1, 'snmp://{{IP_ADDRR}}:{{PORT}}/status',
                '{{RESP}}', NULL, usersrow.login, usersrow.login);
        /*







        INSERT



        --RAISE NOTICE 'arrow.a = (%) ', arrow.num1;

        --INSERT INTO device_type(model, description, vendor, device_class_id, utility_path, interface_id, port_speed, data_bits, is_parity, is_flow_control, uart_driver_type, is_hw_virtual_port_use, is_termo_connected, configuration_id, creator_code, changer_code)


        INSERT INTO teco.scenario_step_params (sname, description, type_id, param_order_no) VALUES
        ()

       TRUNCATE TABLE  teco.scenarios CASCADE;

        INSERT INTO teco.scenarios(command_id, sname, step, request_pattern, responce_pattern, description)
        VALUES ()


*/
    END;

$$ LANGUAGE plpgsql;


SELECT *
FROM teco.device_class;

SELECT *
FROM teco.device_type;

SELECT *
FROM commands



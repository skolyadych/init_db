/*
Uesers Table
v 1.0
*/
DROP TABLE IF EXISTS teco.users CASCADE;

CREATE TABLE teco.users
(
    id          SERIAL       NOT NULL PRIMARY KEY, --primary key
    login       VARCHAR(128) NOT NULL,
    first_name  VARCHAR(128) NOT NULL,
    last_name   VARCHAR(128) NOT NULL,
    middle_name VARCHAR(128) NULL,
    group_code  VARCHAR(32)  NOT NULL REFERENCES teco.user_roles (code),
    position    VARCHAR(512) NULL,
    created     TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_id  INTEGER      NOT NULL    DEFAULT 1,
    chaged      TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    changer_id  INTEGER      NOT NULL    DEFAULT 0,
    is_deleted  NUMERIC(1, 0)            DEFAULT 0,
    CONSTRAINT uq_users_name UNIQUE (login)
);

CREATE UNIQUE INDEX ix_users_login ON teco.users (login);
CREATE INDEX ix_users_group_id ON teco.users (group_code);

GRANT ALL ON teco.users TO teco_users;

INSERT INTO users(login, first_name, last_name, middle_name, position, group_code)
VALUES ('admin', 'Sergey', 'Kolyadich', 'Arkadievich', 'Developper', 'owner');

INSERT INTO users(login, first_name, last_name, middle_name, position, group_code)
VALUES ('teco', 'creator', 'teco', 'teco', 'dbowner', 'owner');


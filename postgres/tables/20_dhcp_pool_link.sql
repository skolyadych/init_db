/*
dhcp_pool_link
v 1.0
S.K.
*/

DROP TABLE IF EXISTS teco.dhcp_pool_link CASCADE;

CREATE TABLE teco.dhcp_pool_link
(
    id           SERIAL
        CONSTRAINT dhcp_pool_link_pk
            PRIMARY KEY,
    code         VARCHAR(32)  NOT NULL,
    netmask      VARCHAR(128) NOT NULL,
    bits         VARCHAR(32)  NOT NULL,
    gateway      VARCHAR(32)  NOT NULL,
    lease_time   INT          NOT NULL,
    region_code  VARCHAR(32)  NOT NULL REFERENCES teco.regions (code),
    created      TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code VARCHAR(32)  NOT NULL REFERENCES users (login),
    is_deleted   NUMERIC(1, 0)            DEFAULT 0
);

COMMENT ON TABLE dhcp_pool_link IS 'L3 сети  регионов';

COMMENT ON COLUMN dhcp_pool_link.id IS 'PrimaryKey';

COMMENT ON COLUMN dhcp_pool_link.netmask IS 'маска сети';

COMMENT ON COLUMN dhcp_pool_link.bits IS 'количество адресов ';
COMMENT ON COLUMN dhcp_pool_link.gateway IS 'шлюз';
COMMENT ON COLUMN dhcp_pool_link.lease_time IS 'время оренды';

CREATE UNIQUE INDEX dhcp_pool_link_code_uindex
    ON dhcp_pool_link (code);

CREATE INDEX dhcp_pool_link_netmask_uindex
    ON dhcp_pool_link (netmask);


GRANT ALL ON dhcp_pool_link TO teco_users;
/*
device_type
v 1.0
S.K.
*/
DROP TABLE IF EXISTS teco.device_type CASCADE;


CREATE TABLE device_type
(
    id                     SERIAL       NOT NULL,
    code                   VARCHAR(32)  NOT NULL,
    model                  VARCHAR(512) NOT NULL,
    description            VARCHAR(512),
    vendor                 VARCHAR(128),
    device_class_code      VARCHAR(32)  NOT NULL
        CONSTRAINT fq_device_class_code
            REFERENCES teco.device_class (code),
    utility_path           VARCHAR(1024),
    interface_code         VARCHAR(32)  NOT NULL REFERENCES teco.interface_type (code),
    port_speed             INT                      DEFAULT 0,
    data_bits              VARCHAR(32),
    is_parity              NUMERIC(1)               DEFAULT 0,
    is_flow_control        NUMERIC(1)               DEFAULT 0,
    uart_driver_type       VARCHAR(32),
    is_hw_virtual_port_use NUMERIC(1)   NOT NULL,
    is_termo_connected     NUMERIC(1)               DEFAULT 0,
    configuration_code     VARCHAR(32)  NOT NULL REFERENCES teco.device_config (code),
    created                TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code           VARCHAR(32)  NOT NULL REFERENCES users (login),
    chaged                 TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    changer_code           VARCHAR(32)  NOT NULL REFERENCES users (login),
    is_deleted             NUMERIC(1, 0)            DEFAULT 0
);

COMMENT ON TABLE device_type IS 'Типы устройств (шаблоны)';

COMMENT ON COLUMN device_type.id IS 'Primary key';

COMMENT ON COLUMN device_type.model IS 'Модель устройства';

COMMENT ON COLUMN device_type.description IS 'Описание модели';

COMMENT ON COLUMN device_type.vendor IS 'Производитель';

COMMENT ON COLUMN device_type.device_class_code IS 'id Класса устройств';

COMMENT ON COLUMN device_type.utility_path IS 'vendor utility path';

COMMENT ON COLUMN device_type.interface_code IS 'Интерфес порта устройства';

COMMENT ON COLUMN device_type.port_speed IS 'Скорость порта устройства бит\сек';

COMMENT ON COLUMN device_type.data_bits IS 'Скорость бит\сек';

COMMENT ON COLUMN device_type.is_parity IS 'Парность';

COMMENT ON COLUMN device_type.is_flow_control IS 'Управление потоком (да\нет)';

COMMENT ON COLUMN device_type.uart_driver_type IS 'Тип драйвера UART';

COMMENT ON COLUMN device_type.is_hw_virtual_port_use IS 'Использовать виртуальный HW порт';

COMMENT ON COLUMN device_type.is_termo_connected IS 'Подключение термодавача';

COMMENT ON COLUMN device_type.configuration_code IS 'id Конфигурации для типа оборудования';

CREATE UNIQUE INDEX device_type_id_uindex
    ON device_type (id);

CREATE UNIQUE INDEX device_type_code_uindex
    ON device_type (code);
CREATE INDEX device_type_model_uindex
    ON device_type (model);

ALTER TABLE device_type
    ADD CONSTRAINT device_type_pk
        PRIMARY KEY (id);

GRANT ALL ON teco.users TO teco_users;
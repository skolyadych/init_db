/*
regions
v 1.0
S.K.
*/

DROP TABLE IF EXISTS teco.regions CASCADE;

CREATE TABLE teco.regions
(
    id                  SERIAL
        CONSTRAINT regions_pk
            PRIMARY KEY,
    code                VARCHAR(32)  NOT NULL,
    sname               VARCHAR(128) NOT NULL,
    description         VARCHAR(512),
    kod_obl             VARCHAR(32),
    created             TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code        VARCHAR(32)  NOT NULL REFERENCES users (login),
    is_deleted          NUMERIC(1, 0)            DEFAULT 0
);

COMMENT ON TABLE regions IS 'Коды регионов';

COMMENT ON COLUMN regions.id IS 'PrimaryKey';

COMMENT ON COLUMN regions.sname IS  'Короткое название';

COMMENT ON COLUMN regions.description IS 'Описание ';
COMMENT ON COLUMN regions.kod_obl IS 'Код во укрпочты';

CREATE UNIQUE INDEX regions_code_uindex
    ON regions (code);

CREATE INDEX regions_extode_uindex
    ON regions (kod_obl);


GRANT ALL ON regions TO teco_users;
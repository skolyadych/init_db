/*
scenario_step_params
v 1.0
S.K.
*/

DROP TABLE IF EXISTS teco.scenario_step_params;

CREATE TABLE scenario_step_params
(
    id             SERIAL
        CONSTRAINT scenario_step_params_pk
            PRIMARY KEY,
    code           VARCHAR(32)                        NOT NULL UNIQUE,
    sname          VARCHAR(128)                       NOT NULL,
    description    VARCHAR(512),
    scenario_code  VARCHAR(32)                        NOT NULL REFERENCES teco.scenarios (code),
    type_code      VARCHAR(32)                        NOT NULL REFERENCES teco.param_types (code),
    is_mandatory   NUMERIC(1, 1)            DEFAULT 0 NOT NULL,
    param_order_no INT                                NOT NULL,
    created        TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code   VARCHAR(32)                        NOT NULL REFERENCES users (login),
    chaged         TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    changer_code   VARCHAR(32)                        NOT NULL REFERENCES users (login),
    is_deleted     NUMERIC(1, 0)            DEFAULT 0,
    CONSTRAINT uq_param_order_scenario UNIQUE (scenario_code, param_order_no)
);

CREATE UNIQUE INDEX ix_sparams_code ON teco.scenario_step_params (code);

COMMENT ON TABLE scenario_step_params IS 'Параметры шагов сценрия';

COMMENT ON COLUMN scenario_step_params.id IS 'PrimaryKey';

COMMENT ON COLUMN scenario_step_params.sname IS 'Название параметра';

COMMENT ON COLUMN scenario_step_params.description IS 'Описание параметра';

COMMENT ON COLUMN scenario_step_params.scenario_code IS 'Привязка к сценарию';

COMMENT ON COLUMN scenario_step_params.type_code IS 'Тип параметра';

COMMENT ON COLUMN scenario_step_params.is_mandatory IS 'Обязательность параметра';

CREATE INDEX scenario_step_params_sname_uindex
    ON scenario_step_params (sname);

GRANT ALL ON scenario_step_params TO teco_users;
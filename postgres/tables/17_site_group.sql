/*
site_group
v 1.0
S.K.
*/

DROP TABLE IF EXISTS teco.site_group CASCADE;

CREATE TABLE teco.site_group
(
    id           SERIAL       NOT NULL
        CONSTRAINT site_group_pk
            PRIMARY KEY,
    code         VARCHAR(32)  NOT NULL UNIQUE,
    sname        VARCHAR(128) NOT NULL,
    description  VARCHAR(512),
    created      TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code VARCHAR(32)  NOT NULL REFERENCES users (login),
    chaged       TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    changer_code VARCHAR(32)  NOT NULL REFERENCES users (login),
    is_deleted   NUMERIC(1, 0)            DEFAULT 0
);


ALTER TABLE site_group
    OWNER TO teco;

CREATE UNIQUE INDEX site_code_group_uindex
    ON site_group (code);

GRANT ALL ON site_group TO teco_users;
/*
param_types
v 1.0
S.K.
*/

DROP TABLE IF EXISTS teco.param_types CASCADE;

CREATE TABLE param_types
(
    id           SERIAL
        CONSTRAINT param_types_pk
            PRIMARY KEY,
    code         VARCHAR(32) UNIQUE NOT NULL,
    sname        VARCHAR(128)       NOT NULL,
    description  VARCHAR(512),
    created      TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code VARCHAR(32)        NOT NULL REFERENCES users (login),
    is_deleted   NUMERIC(1, 0)            DEFAULT 0
);
CREATE UNIQUE INDEX ix_param_type_code ON teco.param_types (code);

COMMENT ON TABLE param_types IS 'Типы параметров';

COMMENT ON COLUMN param_types.id IS 'PrimaryKey';

COMMENT ON COLUMN param_types.sname IS 'Название параметра';

COMMENT ON COLUMN param_types.description IS 'Описание параметра';

CREATE  INDEX param_types_sname_uindex
    ON param_types (sname);

GRANT ALL ON param_types TO teco_users;
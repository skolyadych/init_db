/*
Ueser Roles
v 1.0
*/
DROP TABLE IF EXISTS teco.user_roles CASCADE;

CREATE TABLE user_roles
(
    id           SERIAL       NOT NULL
        CONSTRAINT user_roles_pk
            PRIMARY KEY,
    code         VARCHAR(32)  NOT NULL,
    role_name    VARCHAR(128) NOT NULL,
    domain_group VARCHAR(512) NULL,
    description  VARCHAR(512) NULL,
    created      TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code VARCHAR(32)  NOT NULL, --REFERENCES users(login)   ,
    chaged       TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    changer_code VARCHAR(32)  NOT NULL,
    is_deleted   NUMERIC(1, 0)            DEFAULT 0
);

COMMENT ON TABLE user_roles IS 'Группы польователей';

COMMENT ON COLUMN user_roles.id IS 'Primary key';

COMMENT ON COLUMN user_roles.role_name IS 'Role name';

COMMENT ON COLUMN user_roles.domain_group IS 'ActiveDirectory group';

COMMENT ON COLUMN user_roles.description IS 'RoleDescription';

ALTER TABLE user_roles
    OWNER TO teco;

GRANT ALL ON teco.user_roles TO teco_users;

CREATE UNIQUE INDEX user_roles_role_code
    ON user_roles (code);

CREATE UNIQUE INDEX user_roles_role_name_uindex
    ON user_roles (role_name);

INSERT INTO teco.user_roles (code, role_name, domain_group, description, creator_code, changer_code)
VALUES ('owner', 'teco_owner', 'KS_Teco_owners', 'Teco Developpers', 'owner', 'owner');

SELECT *
FROM teco.user_roles
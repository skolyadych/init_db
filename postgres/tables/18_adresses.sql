/*
address
v 1.0
S.K.
*/

DROP TABLE IF EXISTS teco.address CASCADE;

CREATE TABLE teco.address
(
    id           SERIAL       NOT NULL
        CONSTRAINT address_pk
            PRIMARY KEY,
    code         VARCHAR(32)  NOT NULL UNIQUE,
    sname        VARCHAR(128) NOT NULL,
    description  VARCHAR(512),
    location     VARCHAR(64),
    postcode     VARCHAR(64),
    region_code  VARCHAR(32)  NOT NULL REFERENCES teco.regions (code),
    town         VARCHAR(128),
    street       VARCHAR(128),
    house        VARCHAR(32),
    apart        VARCHAR(32),
    room         VARCHAR(32),
    created      TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code VARCHAR(32)  NOT NULL REFERENCES users (login),
    chaged       TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    changer_code VARCHAR(32)  NOT NULL REFERENCES users (login),
    is_deleted   NUMERIC(1, 0)            DEFAULT 0
);



ALTER TABLE site_group
    OWNER TO teco;

CREATE UNIQUE INDEX address_uindex
    ON address (code);

GRANT ALL ON address TO teco_users;
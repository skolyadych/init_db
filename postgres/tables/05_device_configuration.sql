/*
device_configuration
v 1.0
S.K.
*/
DROP TABLE IF EXISTS teco.device_config CASCADE;



CREATE TABLE device_config
(
    id           SERIAL
        CONSTRAINT device_config_pk
            PRIMARY KEY,
    code         VARCHAR(32)  NOT NULL,
    sname        VARCHAR(128) NOT NULL,
    description  VARCHAR(512),
    created      TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code VARCHAR(32)  NOT NULL REFERENCES users (login),
    chaged       TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    changer_code VARCHAR(32)  NOT NULL REFERENCES users (login),
    is_deleted   NUMERIC(1, 0)            DEFAULT 0

);

COMMENT ON TABLE device_config IS 'Конфигурации типов устройсты';

COMMENT ON COLUMN device_config.id IS 'Primary Key';

COMMENT ON COLUMN device_config.sname IS 'ShortName';

COMMENT ON COLUMN device_config.description IS 'Описание конфигурации';

CREATE UNIQUE INDEX device_config_code_uindex
    ON device_config (code);

GRANT ALL ON teco.device_config TO teco_users;
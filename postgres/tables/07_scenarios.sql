/*
commands
v 1.0
S.K.
*/

DROP TABLE IF EXISTS teco.scenarios CASCADE;

CREATE TABLE scenarios
(
    id               SERIAL
        CONSTRAINT scenarios_pk
            PRIMARY KEY,
    command_code     VARCHAR(32)  NOT NULL REFERENCES teco.commands (code),
    code             VARCHAR(32)  NOT NULL,
    sname            VARCHAR(128) NOT NULL,
    step             INT          NOT NULL,
    request_pattern  VARCHAR(2048),
    responce_pattern VARCHAR(2048),
    description      VARCHAR(512),
    created          TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code     VARCHAR(32)  NOT NULL REFERENCES users (login),
    chaged           TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    changer_code     VARCHAR(32)  NOT NULL REFERENCES users (login),
    is_deleted       NUMERIC(1, 0)            DEFAULT 0,
    CONSTRAINT uq_scenarios_order_com UNIQUE (command_code, step)
);

CREATE UNIQUE INDEX ix_scenarios_code ON scenarios (code);

COMMENT ON TABLE scenarios IS 'Сценарии комманд';

COMMENT ON COLUMN scenarios.id IS 'Primary key';

COMMENT ON COLUMN scenarios.command_code IS 'Reference to commands';

COMMENT ON COLUMN scenarios.sname IS 'Название шага сценария';

COMMENT ON COLUMN scenarios.step IS 'Шаг сценария';

COMMENT ON COLUMN scenarios.request_pattern IS 'Request pattern';

COMMENT ON COLUMN scenarios.responce_pattern IS 'Responce pattern';

COMMENT ON COLUMN scenarios.description IS 'Описание шага';


GRANT ALL ON teco.scenarios TO teco_users;
/*
alarm_status
v 1.0
S.K.
*/

DROP TABLE IF EXISTS teco.alarm_type CASCADE ;

CREATE TABLE alarm_type
(
    id                  SERIAL
        CONSTRAINT alarm_type_pk
            PRIMARY KEY,
    code                VARCHAR(32)  NOT NULL,
    sname               VARCHAR(128) NOT NULL,
    description         VARCHAR(512),
    external_code       VARCHAR(32),
    alarm_category_code VARCHAR(32) REFERENCES alarm_category (code),
    created             TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code        VARCHAR(32)  NOT NULL REFERENCES users (login),
    is_deleted          NUMERIC(1, 0)            DEFAULT 0
);

COMMENT ON TABLE alarm_type IS 'Типы аварий';

COMMENT ON COLUMN alarm_type.id IS 'PrimaryKey';

COMMENT ON COLUMN alarm_type.sname IS 'Название аварии';

COMMENT ON COLUMN alarm_type.description IS 'Описание аварии';
COMMENT ON COLUMN alarm_type.external_code IS 'Код во внешней системе';

CREATE UNIQUE INDEX alarm_type_code_uindex
    ON alarm_type (code);

CREATE INDEX alarm_type_extode_uindex
    ON alarm_type (external_code);


GRANT ALL ON alarm_type TO teco_users;
/*
device_class
v 1.0
S.K.
*/
DROP TABLE IF EXISTS teco.device_class CASCADE;


CREATE TABLE teco.device_class
(
    id           SERIAL       NOT NULL,
    code         VARCHAR(32)  NOT NULL,
    sname        VARCHAR(128) NOT NULL,
    description  VARCHAR(512),
    order_no     SERIAL       NOT NULL,
    request_rate BIGINT                   DEFAULT 0,
    created      TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code VARCHAR(32)  NOT NULL REFERENCES users (login),
    chaged       TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    changer_code VARCHAR(32)  NOT NULL REFERENCES users (login),
    is_deleted   NUMERIC(1, 0)            DEFAULT 0
);

COMMENT ON TABLE device_class IS 'Классы устройств';

COMMENT ON COLUMN device_class.id IS 'Primary key';

COMMENT ON COLUMN device_class.sname IS 'Короткое имя для дерева меню';

COMMENT ON COLUMN device_class.description IS 'Описание класса устройств (для всплывающих подсказок';

COMMENT ON COLUMN device_class.order_no IS 'Порядковый номер (для сортировки)';

COMMENT ON COLUMN device_class.request_rate IS 'Частота опроса по умолчанию в секундах (0-не опрашивать)';

CREATE UNIQUE INDEX device_class_id_uindex
    ON device_class (id);

CREATE UNIQUE INDEX device_class_order_no_uindex
    ON device_class (order_no);

CREATE UNIQUE INDEX device_class_sname_uindex
    ON device_class (code);

ALTER TABLE device_class
    ADD CONSTRAINT device_class_pk
        PRIMARY KEY (id);

GRANT ALL ON teco.users TO teco_users;
/*
commands
v 1.0
S.K.
*/
DROP TABLE IF EXISTS teco.commands CASCADE ;

CREATE TABLE commands
(
    id                 SERIAL
        CONSTRAINT commands_pk
            PRIMARY KEY,
    code               VARCHAR(32)  NOT NULL UNIQUE ,
    configuration_code VARCHAR(32)  NOT NULL REFERENCES teco.device_config (code),
    direction          NUMERIC(1)               DEFAULT 0,
    order_no           INTEGER      NOT NULL,
    sname              VARCHAR(128) NOT NULL,
    description        VARCHAR(512),
    created            TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code       VARCHAR(32)  NOT NULL REFERENCES users (login),
    chaged             TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    changer_code       VARCHAR(32)  NOT NULL REFERENCES users (login),
    is_deleted         NUMERIC(1, 0)            DEFAULT 0,
    CONSTRAINT uq_config_order UNIQUE (configuration_code, order_no)
);

CREATE UNIQUE INDEX ix_command_code ON teco.device_config (code);

COMMENT ON TABLE commands IS 'Комманды для управления устройствами';

COMMENT ON COLUMN commands.id IS 'Primary Key';

COMMENT ON COLUMN commands.id IS 'Primary Key';

COMMENT ON COLUMN commands.direction IS 'Направление комманды
0-входящая
1-исходящая';

COMMENT ON COLUMN commands.order_no IS 'Порядок выполнения';
COMMENT ON COLUMN device_config.sname IS 'ShortName';

COMMENT ON COLUMN device_config.description IS 'Описание конфигурации';

GRANT ALL ON teco.commands TO teco_users;
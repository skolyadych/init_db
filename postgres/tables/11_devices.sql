/*
device
v 1.0
S.K.
*/
DROP TABLE IF EXISTS teco.device CASCADE;


CREATE TABLE device
(
    id                     SERIAL       NOT NULL,
    code                   VARCHAR(32)  NOT NULL UNIQUE,
    sname                  VARCHAR(512) NOT NULL,
    description            VARCHAR(512),
    device_class_code      VARCHAR(32)  NOT NULL
        CONSTRAINT fq_device_class_code
            REFERENCES teco.device_class (code),
    device_type_code       VARCHAR(32)  NOT NULL
        CONSTRAINT fq_device_type_code
            REFERENCES teco.device_type (code),
    interface_code         VARCHAR(32)  NOT NULL
        CONSTRAINT fq_interface_code
            REFERENCES teco.interface_type (code),
    utility_path           VARCHAR(1024),
    port_speed             INT                      DEFAULT 0,
    data_bits              VARCHAR(32),
    is_parity              NUMERIC(1)               DEFAULT 0,
    is_flow_control        NUMERIC(1)               DEFAULT 0,
    uart_driver_type       VARCHAR(32),
    is_hw_virtual_port_use NUMERIC(1)   NOT NULL,
    is_termo_connected     NUMERIC(1)               DEFAULT 0,
    serial_no              VARCHAR(128) NOT NULL,
    inventory_no           VARCHAR(128) NOT NULL,
    created                TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code           VARCHAR(32)  NOT NULL REFERENCES users (login),
    chaged                 TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    changer_code           VARCHAR(32)  NOT NULL REFERENCES users (login),
    is_deleted             NUMERIC(1, 0)            DEFAULT 0
);

COMMENT ON TABLE device IS 'Эксплуатируемые устройства';

COMMENT ON COLUMN device.id IS 'Primary key';

COMMENT ON COLUMN device.sname IS 'Название устройства';

COMMENT ON COLUMN device.description IS 'Описание устройства';


COMMENT ON COLUMN device.device_class_code IS 'id Класса устройств';

COMMENT ON COLUMN device.utility_path IS 'vendor utility path';

COMMENT ON COLUMN device.interface_code IS 'Интерфес порта устройства';

COMMENT ON COLUMN device.port_speed IS 'Скорость порта устройства бит\сек';

COMMENT ON COLUMN device.data_bits IS 'Скорость бит\сек';

COMMENT ON COLUMN device.is_parity IS 'Парность';

COMMENT ON COLUMN device.is_flow_control IS 'Управление потоком (да\нет)';

COMMENT ON COLUMN device.uart_driver_type IS 'Тип драйвера UART';

COMMENT ON COLUMN device.is_hw_virtual_port_use IS 'Использовать виртуальный HW порт';

COMMENT ON COLUMN device.is_termo_connected IS 'Подключение термодавача';


COMMENT ON COLUMN device.serial_no IS 'Serial number';
COMMENT ON COLUMN device.inventory_no IS 'Inventory number';


CREATE UNIQUE INDEX device_id_uindex
    ON device (id);

CREATE UNIQUE INDEX ix_device_code
    ON device (code);

ALTER TABLE device
    ADD CONSTRAINT device_pk
        PRIMARY KEY (id);

GRANT ALL ON teco.users TO teco_users;
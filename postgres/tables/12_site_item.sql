/*
site_item
v 1.0
S.K.
*/
DROP TABLE IF EXISTS teco.site_item;



CREATE TABLE site_item
(
    id              SERIAL
        CONSTRAINT site_item_pk
            PRIMARY KEY,
    code            VARCHAR(32)  NOT NULL,
    site_code       VARCHAR(32)  NOT NULL REFERENCES site (code),
    port_no         INT          NOT NULL,
    device_code     VARCHAR(32)  NOT NULL REFERENCES device (code),
    interface_code  VARCHAR(32)  NOT NULL
        CONSTRAINT fq_interface_code2
            REFERENCES teco.interface_type (code),
    status_code     VARCHAR(32)  NOT NULL REFERENCES device_status (code),
    last_alarm_code INT          NOT NULL,
    ip_address      VARCHAR(128) NOT NULL,
    ip_pool_code    VARCHAR(32)  NOT NULL REFERENCES teco.dhcp_pool_link (code),
    created         TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code    VARCHAR(32)  NOT NULL REFERENCES users (login),
    chaged          TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    changer_code    VARCHAR(32)  NOT NULL REFERENCES users (login),
    is_deleted      NUMERIC(1, 0)            DEFAULT 0

);

COMMENT ON TABLE site_item IS 'Устройства в шкафу';

COMMENT ON COLUMN site_item.id IS 'Primary Key';

COMMENT ON COLUMN site_item.site_code IS 'Reference to site';

COMMENT ON COLUMN site_item.port_no IS 'Number of port on device';

COMMENT ON COLUMN site_item.device_code IS 'Устройство в шкафу';

COMMENT ON COLUMN site_item.status_code IS 'Ид статуса устройства';

COMMENT ON COLUMN site_item.last_alarm_code IS 'Последний тип авариии';

CREATE UNIQUE INDEX site_item_device_id_uindex
    ON site_item (device_code);
CREATE UNIQUE INDEX site_item_siteid_portno
    ON site_item (status_code, port_no);

GRANT ALL ON teco.site_item TO teco_users;
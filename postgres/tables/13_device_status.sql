/*
device_status
v 1.0
S.K.
*/

DROP TABLE IF EXISTS teco.device_status CASCADE;

CREATE TABLE device_status
(
    id           SERIAL
        CONSTRAINT device_status_pk
            PRIMARY KEY,
    code         VARCHAR(32) UNIQUE NOT NULL,
    sname        VARCHAR(128)       NOT NULL,
    description  VARCHAR(512),
    created      TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code VARCHAR(32)        NOT NULL REFERENCES users (login),
    is_deleted   NUMERIC(1, 0)            DEFAULT 0
);

COMMENT ON TABLE device_status IS 'Типы статусов устройств';

COMMENT ON COLUMN device_status.id IS 'PrimaryKey';

COMMENT ON COLUMN device_status.sname IS 'Название статуса';

COMMENT ON COLUMN device_status.description IS 'Описание статуса';

CREATE UNIQUE INDEX device_status_code_uindex
    ON device_status (code);

GRANT ALL ON device_status TO teco_users;
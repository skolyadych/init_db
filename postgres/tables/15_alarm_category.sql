/*
alarm_category
v 1.0
S.K.
*/

DROP TABLE IF EXISTS teco.alarm_category CASCADE ;

CREATE TABLE alarm_category
(
    id             SERIAL
        CONSTRAINT alarm_category_pk
            PRIMARY KEY,
    code           VARCHAR(32) UNIQUE NOT NULL,
    sname          VARCHAR(128)       NOT NULL,
    description    VARCHAR(512),
    reaction_level INT                NOT NULL,
    created        TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    creator_code   VARCHAR(32)        NOT NULL REFERENCES users (login),
    is_deleted     NUMERIC(1, 0)            DEFAULT 0
);

COMMENT ON TABLE alarm_category IS 'Справочник категорий аварий';

COMMENT ON COLUMN alarm_category.id IS 'PrimaryKey';

COMMENT ON COLUMN alarm_category.sname IS 'Название категории';

COMMENT ON COLUMN alarm_category.description IS 'Описание категории';


CREATE UNIQUE INDEX alarm_category_code_uindex
    ON alarm_category (code);

GRANT ALL ON alarm_category TO teco_users;